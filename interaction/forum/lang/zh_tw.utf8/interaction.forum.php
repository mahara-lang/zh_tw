<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage interaction-forum
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

$string['addpostsuccess'] = '成功發布帖子';
$string['addtitle'] = '新增論壇';
$string['addtopic'] = '新增主題';
$string['addtopicsuccess'] = '成功發布主題';
$string['autosubscribeusers'] = '自動加入用戶？';
$string['autosubscribeusersdescription'] = '選擇是否將群組用戶自動加入此論壇';
$string['Body'] = '內文';
$string['cantaddposttoforum'] = '您不可以發布此論壇';
$string['cantaddposttotopic'] = '您不可以發布此主題';
$string['cantaddtopic'] = '您不可以在此論壇發布主題';
$string['cantdeletepost'] = '您不可以在此論壇刪除帖子';
$string['cantdeletethispost'] = '您不可以刪除此帖子';
$string['cantdeletetopic'] = '您不可以在此論壇刪除主題';
$string['canteditpost'] = '您不可以編輯此帖子';
$string['cantedittopic'] = '您不可以編輯此主題';
$string['cantfindforum'] = '無法找到 %s論壇';
$string['cantfindpost'] = '無法找到 %s帖子';
$string['cantfindtopic'] = '無法找到 %s主題';
$string['cantviewforums'] = '您不可以在此群組中檢視論壇';
$string['cantviewtopic'] = '您不可以在此群組中檢視主題';
$string['chooseanaction'] = '選擇動作';
$string['clicksetsubject'] = '點擊確定一個主題';
$string['Closed'] = '已關閉';
$string['Close'] = '關閉';
$string['closeddescription'] = '已關閉的主題只能由版主和工作組管理員回覆';
$string['Count'] = '點撃數';
$string['createtopicusersdescription'] = '如果設定為「所有群組會員」，所有人都可以新增主題和回覆已有的主題。如果設定為「只有版主和群組管理員」，就只有版主和群組管理員可以新增主題，但如果主題已存在，所有會員仍然能夠回覆該主題。';
$string['currentmoderators'] = '當前版主';
$string['deleteforum'] = '刪除論壇';
$string['deletepost'] = '刪除帖子';
$string['deletepostsuccess'] = '成功刪除帖子';
$string['deletepostsure'] = '您確定嗎？它不能被取消。';
$string['deletetopic'] = '刪除主題';
$string['deletetopicvariable'] = '刪除主題 \'%s\'';
$string['deletetopicsuccess'] = '成功刪除主題';
$string['deletetopicsure'] = '您確定嗎？它不能被取消。';
$string['editpost'] = '編輯帖子';
$string['editpostsuccess'] = '成功編輯帖子';
$string['editstothispost'] = '編輯此帖:';
$string['edittitle'] = '編輯論壇';
$string['edittopic'] = '編輯主題';
$string['edittopicsuccess'] = '成功編輯主題';
$string['forumname'] = '論壇名稱';
$string['forumposttemplate'] = "%s 由 %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
要到線上檢視和回覆這一個帖，請到這個連結:
%s

如要取消訂閱論壇 %s, 請到:
%s";
$string['forumsuccessfulsubscribe'] = '成功加入論壇';
$string['forumsuccessfulunsubscribe'] = '成功退出論壇';
$string['gotoforums'] = '前往論壇';
$string['groupadmins'] = '群組管理員';
$string['groupadminlist'] = '群組管理員:';
$string['Key'] = '圖例';
$string['lastpost'] = '最後發表的帖子';
$string['latestforumposts'] = '最新論壇帖子';
$string['Moderators'] = '版主';
$string['moderatorsandgroupadminsonly'] = '只有版主和群組管理員';
$string['moderatorslist'] = '版主:';
$string['moderatorsdescription'] = '版主可以編輯和刪除主題和帖子。他們還可以打開，關閉，設定和取消主題置頂';
$string['name'] = '論壇';
$string['nameplural'] = '論壇';
$string['newforum'] = '新論壇';
$string['newforumpostby'] = '%s: %s: 發表新論壇帖子 %s';
$string['newpost'] = '新帖子: ';
$string['newtopic'] = '新主題';
$string['noforumpostsyet'] = '這一群組中尚未有帖子';
$string['noforums'] = '這群組中沒有論壇';
$string['notopics'] = '這群組中沒有主題';
$string['Open'] = '開啟';
$string['Order'] = '順序';
$string['orderdescription'] = '選擇如何將此論壇相及其他論壇來排序';
$string['Post'] = '帖子';
$string['postbyuserwasdeleted'] = '%s 發表的帖子已被刪除';
$string['postedin'] = '%s 在 %s 發表';
$string['Poster'] = '發帖者';
$string['postreply'] = '回覆帖子';
$string['Posts'] = '帖子';
$string['postsvariable'] = '帖子: %s';
$string['potentialmoderators'] = '候選版主';
$string['re'] ='回覆: %s';
$string['regulartopics'] = '經常性主題';
$string['Reply'] = '回覆';
$string['replyto'] = '回覆: ';
$string['replytotopicby'] = '%s: %s:  "%s" 回覆 %s';
$string['Sticky'] = '置頂';
$string['stickydescription'] = '置頂的主題將被放在每一頁的頂部';
$string['stickytopics'] = '置頂主題';
$string['Subscribe'] = '加入';
$string['Subscribed'] = '已加入';
$string['subscribetoforum'] = '加入論壇';
$string['subscribetotopic'] = '加入主題';
$string['Subject'] = '話題';
$string['Topic'] = '主題';
$string['Topics'] = '主題';
$string['topiclower'] = '主題';
$string['topicslower'] = '主題';
$string['topicclosedsuccess'] = '成功關閉主題';
$string['topicisclosed'] = '這個主題已關閉。只有版主和工作組管理員可以張貼新回覆';
$string['topicopenedsuccess'] = '成功開啟主題';
$string['topicstickysuccess'] = '主題已成功設定為置頂';
$string['topicsubscribesuccess'] = '成功加入主題';
$string['topicunstickysuccess'] = '主題已成功取消置頂';
$string['topicunsubscribesuccess'] = '成功取消訂閱主題';
$string['topicupdatefailed'] = '主題更新失敗';
$string['typenewpost'] = '新論壇帖子';
$string['Unsticky'] = '取消置頂';
$string['Unsubscribe'] = '取消訂閱';
$string['unsubscribefromforum'] = '取消訂閱論壇';
$string['unsubscribefromtopic'] = '取消訂閱主題';
$string['updateselectedtopics'] = '更新選定的主題';
$string['whocancreatetopics'] = '誰人可以新增主題';
$string['youcannotunsubscribeotherusers'] = '你不能替其他用戶取消訂閱';
$string['youarenotsubscribedtothisforum'] = '你不能訂閱這個論壇';
$string['youarenotsubscribedtothistopic'] = '你不能訂閱這個主題';

$string['today'] = '今天';
$string['yesterday'] = '昨天';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['strftimerecentfullrelative'] = '%%v, %%l:%%M %%p';

?>