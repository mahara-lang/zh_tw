<?php
/**
 * Creative Commons License Block type for Mahara
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-creativecommons
 * @author     Francois Marier <francois@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2009 Catalyst IT Ltd
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Creative Commons';
$string['description'] = '為你的選集附加一個Creative Commons授權條款';
$string['blockcontent'] = '區塊內容';

$string['alttext'] = 'Creative Commons授權條款';
$string['licensestatement'] = "本作品以 <a rel=\"license\" href=\"%s\">Creative Commons %s 3.0 Unported License</a> 分享";
$string['sealalttext'] = 'This license is acceptable for Free Cultural Works.';

$string['config:noncommercial'] = '是否允許您的作品被使用於商業目的?';
$string['config:noderivatives'] = '是否允許他人衍生您的作品?';
$string['config:sharealike'] = '是，只要其他人也同樣分享';

$string['by'] = '姓名標示';
$string['by-sa'] = '姓名標示-授權分享';
$string['by-nd'] = '姓名標示-非衍生作品';
$string['by-nc'] = '姓名標示-非商業用途';
$string['by-nc-sa'] = '姓名標示-非商業用途-授權分享';
$string['by-nc-nd'] = '姓名標示-非商業用途-非衍生作品';

?>
