<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-wall
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = '牆壁';
$string['otherusertitle'] = "%s 的牆壁";
$string['description'] = '顯示一個可讓別人留下意見的區域';
$string['noposts'] = '牆壁上沒有帖子可以顯示';
$string['makeyourpostprivate'] = '使您的帖子變為私人？';
$string['viewwall'] = '觀看牆壁';
$string['backtoprofile'] = '回到個人檔案';
$string['wall'] = '牆壁';
$string['wholewall'] = '觀看整個牆壁';
$string['reply'] = '回覆';
$string['delete'] = '刪除帖子';
$string['deletepost'] = '刪除帖子';
$string['Post'] = '帖子';
$string['deletepostsure'] = '你確定要刪除帖子? 帖子刪除後不能夠回復。';
$string['deletepostsuccess'] = '已成功刪除帖子';
$string['maxcharacters'] = "每個帖子最多可有 %s 個字。";
$string['sorrymaxcharacters'] = "對不起，你的帖子字數不能多過 %s 個字。";

// Config strings
$string['postsizelimit'] = "帖子大小限制";
$string['postsizelimitdescription'] = "你可以限制帖子的大小。已存在的帖子並不會受影響";
$string['postsizelimitmaxcharacters'] = "最大字數";
$string['postsizelimitinvalid'] = "這不是一個正確的數字。";
$string['postsizelimittoosmall'] = "這個限制不能少過零。";

?>