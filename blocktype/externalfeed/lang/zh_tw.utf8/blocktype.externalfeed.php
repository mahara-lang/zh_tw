<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-externalfeeds
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = '外部訊息來源';
$string['description'] = '嵌入外部RSS或Atom格式的訊息來源';
$string['feedlocation'] = '訊息來源的位置';
$string['feedlocationdesc'] = '一個有效的URL或Atom訊息來源網址';
$string['showfeeditemsinfull'] = '查看所有訊息來源項目？';
$string['showfeeditemsinfulldesc'] = '顯示訊息來源的總結，或是也顯示每一個訊息來源的描述';
$string['invalidurl'] = '該網址是無效的。您只能查看HTTP和HTTPS的訊息來源網址。';
$string['invalidfeed'] = '訊息來源似乎無效。回報的錯誤是: %s';
$string['lastupdatedon'] = '最後更新日期 %s';
$string['defaulttitledescription'] = '如果你把這裡留空，提要標題將被使用';
?>
