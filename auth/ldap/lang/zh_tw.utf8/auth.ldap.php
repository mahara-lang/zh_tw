<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'LDAP';
$string['description'] = 'IMAP電子郵件伺服器';
$string['notusable'] = '請安裝的PHP的LDAP擴展';

$string['contexts'] = '內容';
$string['distinguishedname'] = '已辨別名稱';
$string['hosturl'] = '主機網址';
$string['ldapfieldforemail'] = 'LDAP電子郵件欄';
$string['ldapfieldforfirstname'] = 'LDAP名字欄';
$string['ldapfieldforsurname'] = 'LDAP姓氏欄';
$string['ldapversion'] = 'LDAP版本';
$string['password'] = '密碼';
$string['searchsubcontexts'] = '搜索分內容';
$string['userattribute'] = '用戶屬性';
$string['usertype'] = '用戶類型';
$string['weautocreateusers'] = '我們自動創建用戶';

?>
