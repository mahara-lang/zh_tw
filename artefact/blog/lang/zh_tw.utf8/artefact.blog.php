<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = '博客';

$string['blog'] = '博客';
$string['blogs'] = '博客';
$string['addblog'] = '新增博客';
$string['addpost'] = '新增帖子';
$string['alignment'] = '對齊';
$string['attach'] = '附加';
$string['attachedfilelistloaded'] = '附加檔案清單加載完成';
$string['attachedfiles'] = '已附加的檔案';
$string['attachments'] = '附件';
$string['blogcopiedfromanotherview'] = '注意：此段是從另一個選集複製。您可以移動或刪除它，但你不能更改裡面的內容。';
$string['blogdesc'] = '描述';
$string['blogdescdesc'] = '例，吉爾的經驗與思考的紀錄';
$string['blogdoesnotexist'] = '您要訪問的博客不存在';
$string['blogpostdoesnotexist'] = '您要訪問的帖子不存在';
$string['blogfilesdirdescription'] = '上載的檔案作為博客主題附件';
$string['blogfilesdirname'] = '博客檔案';
$string['blogpost'] = '帖子';
$string['blogdeleted'] = '已被刪除的博客';
$string['blogpostdeleted'] = '帖子已被刪除';
$string['blogpostpublished'] = '帖子已發表';
$string['blogpostsaved'] = '帖子已儲存';
$string['blogsettings'] = '博客設定';
$string['blogtitle'] = '標題';
$string['blogtitledesc'] = '例，吉爾的護理實踐日誌';
$string['border'] = '邊界';
$string['browsemyfiles'] = '瀏覽我的檔案';
$string['cancel'] = '取消';
$string['commentsallowed'] = '加入評論';
$string['commentsalloweddesc'] = ' ';
$string['commentsallowedno'] = '此博客不允許評論';
$string['commentsallowedyes'] = '允許已登錄用戶評論此博客';
$string['commentsnotify'] = '評論通知';
$string['commentsnotifydesc'] = '當有人在你的博客帖子增加評論,您可以選擇接收通知';
$string['commentsnotifyno'] = '當有人在這博客增加評論,請不要通知我';
$string['commentsnotifyyes'] = '當有人在這博客增加評論,請通知我';
$string['createandpublishdesc'] = '這將創建博客帖子，並提供給他人閱讀';
$string['createasdraftdesc'] = '這將創建博客帖子，但它不會被其他人閱讀，直到您選擇將其發表。';
$string['createblog'] = '創建博客';
$string['delete'] = '刪除';
$string['deleteblog?'] = '您確定要刪除此博客？';
$string['deleteblogpost?'] = '您確定要刪除這個帖子？';
$string['description'] = '描述';
$string['dimensions'] = '尺寸';
$string['draft'] = '草稿';
$string['edit'] = '編輯';
$string['editblogpost'] = '編輯帖子';
$string['erroraccessingblogfilesfolder'] = '訪問博客檔案文件夾發生錯誤';
$string['errorsavingattachments'] = '儲存博客帖子附件發生錯誤';
$string['horizontalspace'] = '橫向空間';
$string['insert'] = '插入';
$string['insertimage'] = '插入圖像';
$string['moreoptions'] = '更多選項';
$string['mustspecifytitle'] = '您必須為您的文章指定一個標題';
$string['mustspecifycontent'] = '您必須為您的文章指定內容';
$string['myblogs'] = '我的博客';
$string['name'] = '名稱';
$string['newattachmentsexceedquota'] = '新上傳的文件總大小將超過您的配額。如果你想保存帖子,請先刪除一些附件';
$string['newblog'] = '新博客';
$string['newblogpost'] = '在博客的新博客帖子 "%s"';
$string['nopostsaddone'] = '沒有帖子 %s新增%s!';
$string['noimageshavebeenattachedtothispost'] = '這帖子沒有附加圖像。您需要上載或附加圖片後，然後才能插入';
$string['nofilesattachedtothispost'] = '沒有附加檔案';
$string['noresults'] = '沒有找到博客帖子';
$string['postbody'] = '主題';
$string['postbodydesc'] = ' ';
$string['postedon'] = '發布於';
$string['postedbyon'] = '%s 發布於 %s';
$string['posttitle'] = '標題';
$string['posts'] = '帖子';
$string['publish'] = '發表';
$string['publishfailed'] = '發生錯誤。您的文章沒有發表';
$string['publishblogpost?'] = '您確定要發表這個帖子？';
$string['published'] = '已發表';
$string['remove'] = '刪除';
$string['save'] = '儲存';
$string['saveandpublish'] = '儲存及發表';
$string['saveasdraft'] = '儲存為草稿';
$string['savepost'] = '儲存帖子';
$string['savesettings'] = '儲存設定';
$string['settings'] = '設定';
$string['thisisdraft'] = '這個帖子是一個草稿';
$string['thisisdraftdesc'] = '當您的帖子仍是草稿時，只有你能看見它';
$string['title'] = '標題';
$string['update'] = '更新';
$string['verticalspace'] = '直向空間';
$string['viewblog'] = '查看博客';
$string['youarenottheownerofthisblog'] = '您不是此博客的擁有者';
$string['youarenottheownerofthisblogpost'] = '您不是此帖子的擁有者';
$string['cannotdeleteblogpost'] = '刪除此博客帖子時發生錯誤。';

$string['baseline'] = '底線';
$string['top'] = '頂端';
$string['middle'] = '正中';
$string['bottom'] = '底部';
$string['texttop'] = '文字頂端';
$string['absolutemiddle'] = '絕對正中';
$string['absolutebottom'] = '絕對底部';
$string['left'] = '左';
$string['right'] = '右';

$string['copyfull'] = '其他人也將獲得自己的副本%s';
$string['copyreference'] = '其他人可以在他們的選集中顯示您的%s';
$string['copynocopy'] = '當複製選集時,跳過此分段';

$string['viewposts'] = '已複製帖子 (%s)';
$string['postscopiedfromview'] = '於%s複製帖子';
?>
