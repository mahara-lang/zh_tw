<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-resume
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = '簡歷';

$string['myresume'] = '我的簡歷';
$string['mygoals'] = '我的目標';
$string['myskills'] = '我的技能';
$string['coverletter'] = '求職信';
$string['interest'] = '興趣';
$string['contactinformation'] = '聯繫資料';
$string['personalinformation'] = '個人資料';
$string['dateofbirth'] = '出生日期';
$string['placeofbirth'] = '出生地點';
$string['citizenship'] = '國籍';
$string['visastatus'] = '簽證';
$string['female'] = '女性';
$string['male'] = '男性';
$string['gender'] = '性別';
$string['maritalstatus'] = '婚姻狀況';
$string['resumesaved'] = '簡歷已保存';
$string['resumesavefailed'] = '無法更新您的簡歷';
$string['educationhistory'] = '教育背景';
$string['employmenthistory'] = '就業紀錄';
$string['certification'] = '證書，認證及獎項';
$string['book'] = '書籍和出版物';
$string['membership'] = '專業會員資格';
$string['startdate'] = '開始日期';
$string['enddate'] = '結束日期';
$string['date'] = '日期';
$string['position'] = '職位';
$string['qualification'] = '資格';
$string['description'] = '描述';
$string['title'] = '標題';
$string['employer'] = '雇主';
$string['jobtitle'] = '職稱';
$string['jobdescription'] = '職位描述';
$string['institution'] = '機構';
$string['qualtype'] = '資格類型';
$string['qualname'] = '資格名稱';
$string['qualdescription'] = '資格描述';
$string['contribution'] = '貢獻';
$string['compositedeleteconfirm'] = '您是否確定要刪除此項？';
$string['compositesaved'] = '已成功保存';
$string['compositesavefailed'] = '保存失敗';
$string['compositedeleted'] = '已成功刪除';
$string['backtoresume'] = '回到我的簡歷';
$string['personalgoal'] = '個人目標';
$string['academicgoal'] = '學術目標';
$string['careergoal'] = '職業目標';
$string['personalskill'] = '個人技能';
$string['academicskill'] = '學術技能';
$string['workskill'] = '職業技能';
$string['goalandskillsaved'] = '已成功保存';
$string['resume'] = '簡歷';
$string['current'] = '當前';
$string['moveup'] = '向上移動';
$string['movedown'] = '向下移動';
?>
