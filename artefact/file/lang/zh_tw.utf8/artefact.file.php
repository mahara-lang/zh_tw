<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = '檔案';

$string['sitefilesloaded'] = '網站檔案載入完成';
$string['bytes'] = '位元組';
$string['cannoteditfolder'] = '你沒有權限在此文件夾中增加內容。';
$string['changessaved'] = '變更已儲存';
$string['contents'] = '內容';
$string['copyrightnotice'] = '版權公告';
$string['create'] = '建立';
$string['Created'] = '已建立';
$string['createfolder'] = '建立文件夾';
$string['Date'] = '日期';
$string['defaultquota'] = '默認限額';
$string['defaultquotadescription'] = '你可設定新使用者所擁有的磁碟空間作為限額。 現有使用者的限額將不會改變。';
$string['deletefile?'] = '你確定你希望刪除此檔案嗎?';
$string['deletefolder?'] = '你確定你希望刪除此文件夾嗎?';
$string['Description'] = '敘述';
$string['destination'] = '目的地';
$string['Download'] = '下載';
$string['downloadoriginalversion'] = '下載原始版本';
$string['editfile'] = '編輯檔案';
$string['editfolder'] = '編輯文件夾';
$string['emptyfolder'] = '置空文件夾';
$string['file'] = '檔案';
$string['File'] = '檔案';
$string['filealreadyindestination'] = '你移動的檔案已存在於該文件夾。';
$string['files'] = '檔案';
$string['Files'] = '檔案';
$string['fileexists'] = '檔案已存在';
$string['fileexistsonserver'] = '已存在相同名稱的檔案。';
$string['fileexistsoverwritecancel'] =  '已存在相同名稱的檔案。  你可嘗試使用另一個名稱, 或覆蓋原來的檔案。';
$string['filelistloaded'] = '檔案列表載入完成';
$string['filemoved'] = '成功移動檔案';
$string['filenamefieldisrequired'] = '必須輸入檔案範疇';
$string['fileinstructions'] = '上載你的圖像、文件或其他檔案至你的版面。 如要移動檔案或文件夾, 把它拉下及放在所需文件夾上。';
$string['filethingdeleted'] = '%s 已被刪除';
$string['folder'] = '文件夾';
$string['Folders'] = '文件夾';
$string['foldercreated'] = '文件夾已建立';
$string['groupfiles'] = '群組檔案';
$string['home'] = '主頁';
$string['htmlremovedmessage'] = '你正在使用<a href="%s">%s</a> 瀏覽 <strong>%s</strong>。 以下顯示的檔案已被過濾以移除含惡意內容, 這只是原版的粗略表述。';
$string['htmlremovedmessagenoowner'] = '你正在瀏覽 <strong>%s</strong>.  以下顯示的檔案已被過濾以移除含惡意內容, 這只是原版的粗略表述。';
$string['image'] = '圖像';
$string['lastmodified'] = '最後的修改';
$string['myfiles'] = '我的檔案';
$string['Name'] = '名稱';
$string['namefieldisrequired'] = '必須輸入名稱範疇';
$string['maxuploadsize'] = '最大上傳檔案大小';
$string['movefaileddestinationinartefact'] = '你不可將文件夾放在該文件夾中。';
$string['movefaileddestinationnotfolder'] = '你只可將檔案移至文件夾中。';
$string['movefailednotfileartefact'] = '只有檔案、文件夾及圖像等作品可被移動。';
$string['movefailednotowner'] = '你沒有權限將檔案移至此文件夾中。';
$string['movefailed'] = '移動失敗';
$string['nofilesfound'] = '找不到檔案';
$string['overwrite'] = '覆蓋';
$string['Owner'] = '擁有人';
$string['parentfolder'] = '上層資料夾';
$string['Preview'] = '預覽';
$string['savechanges'] = '儲存變更';
$string['Size'] = '大小';
$string['timeouterror'] = '上傳檔案失敗: 請嘗試再次上傳檔案';
$string['title'] = '名稱';
$string['titlefieldisrequired'] = '必須輸入名稱範疇';
$string['Type'] = '類別';
$string['unlinkthisfilefromblogposts?'] = '這個檔案附加於一個或多於一個博客文章。 如你刪除此檔案, 它亦會從有關文章中移除。';
$string['upload'] = '上傳';
$string['uploadexceedsquota'] = '上傳此檔案會令超過你的磁碟空間限額。 請嘗試刪除某些已上傳的檔案。';
$string['uploadfile'] =  '上傳檔案';
$string['uploadfileexistsoverwritecancel'] =  '已存在相同名稱的檔案。 你可重新命名準備上傳的檔案, 或覆蓋該已存在的檔案。';
$string['uploadingfiletofolder'] =  '正上傳 %s 至 %s';
$string['uploadoffilecomplete'] = '上傳 %s 完成';
$string['uploadoffilefailed'] =  '上傳 %s 失敗';
$string['uploadoffiletofoldercomplete'] = '上傳 %s 至 %s 完成';
$string['uploadoffiletofolderfailed'] = '上傳 %s 至 %s 失敗';
$string['youmustagreetothecopyrightnotice'] = '你必須同意版權公告。';


// File types
$string['ai'] = 'Postscript文件';
$string['aiff'] = 'AIFF 聲音檔案';
$string['application'] = '不知名應用程式';
$string['au'] = 'AU 聲音檔案';
$string['avi'] = 'AVI 影像檔案';
$string['bmp'] = 'Bitmap 圖像';
$string['doc'] = 'MS 文字檔案';
$string['dss'] = 'Digital Speech Standard Sound File';
$string['gif'] = 'GIF 圖像';
$string['html'] = 'HTML 檔案';
$string['jpg'] = 'JPEG 圖像';
$string['jpeg'] = 'JPEG 圖像';
$string['js'] = 'Javascript 檔案';
$string['latex'] = 'LaTeX 文件';
$string['m3u'] = 'M3U 聲音檔案';
$string['mp3'] = 'MP3 聲音檔案';
$string['mp4_audio'] = 'MP4 聲音檔案';
$string['mp4_video'] = 'MP4 影像檔案';
$string['mpeg'] = 'MPEG 影片';
$string['odb'] = 'Openoffice Database';
$string['odc'] = 'Openoffice Calc File';
$string['odf'] = 'Openoffice Formula File';
$string['odg'] = 'Openoffice Graphics File';
$string['odi'] = 'Openoffice Image';
$string['odm'] = 'Openoffice Master Document File';
$string['odp'] = 'Openoffice Presentation';
$string['ods'] = 'Openoffice Spreadsheet';
$string['odt'] = 'Openoffice Document';
$string['oth'] = 'Openoffice Web Document';
$string['ott'] = 'Openoffice Template Document';
$string['pdf'] = 'PDF 文件';
$string['png'] = 'PNG 圖像';
$string['ppt'] = 'MS Powerpoint 文件';
$string['quicktime'] = 'Quicktime 影片';
$string['ra'] = 'Real 聲音檔案';
$string['rtf'] = 'RTF 文件';
$string['sgi_movie'] = 'SGI 影片檔案';
$string['sh'] = 'Shell Script';
$string['tar'] = 'TAR Archive';
$string['gz'] = 'Gzip 壓縮檔案';
$string['bz2'] = 'Bzip2 壓縮檔案';
$string['txt'] = '純文字檔案';
$string['wav'] = 'WAV 聲音檔案';
$string['wmv'] = 'WMV 影片檔案';
$string['xml'] = 'XML 檔案';
$string['zip'] = 'ZIP Archive';
$string['swf'] = 'SWF Flash movie';
$string['flv'] = 'FLV Flash movie';
$string['mov'] = 'MOV Quicktime 影片';
$string['mpg'] = 'MPG 影片';
$string['ram'] = 'RAM Real Player 影片';
$string['rpm'] = 'RPM Real Player 影片';
$string['rm'] = 'RM Real Player 影片';


// Profile icons
$string['profileiconsize'] = '個人檔案圖像大小';
$string['profileicons'] = '個人檔案圖像';
$string['Default'] = '默認';
$string['deleteselectedicons'] = '刪除已選圖像';
$string['profileicon'] = '個人檔案圖像';
$string['noimagesfound'] = '找不到圖像';
$string['uploadedprofileiconsuccessfully'] = '上載新的個人檔案圖像成功';
$string['profileiconsetdefaultnotvalid'] = '不能設定默認個人檔案圖像, 選擇無效。';
$string['profileiconsdefaultsetsuccessfully'] = '默認個人檔案圖像設定成功';
$string['profileiconsdeletedsuccessfully'] = '個人檔案圖像刪除成功';
$string['profileiconsnoneselected'] = '沒有個人檔案圖像被刪除。';
$string['onlyfiveprofileicons'] = '你只可上傳5張個人檔案圖像。';
$string['or'] = '或';
$string['profileiconuploadexceedsquota'] = '上傳此個人檔案圖像會超過你的磁碟空間限額。 請嘗試刪除某些已上傳的檔案。 ';
$string['profileiconimagetoobig'] = '你上傳的圖像太大 (%sx%s pixels). 圖像大小不可超過 %sx%s pixels';
$string['uploadingfile'] = '正在上傳檔案...';
$string['uploadprofileicon'] = '上傳個人檔案圖像';
$string['profileiconsiconsizenotice'] = '你最多只可上傳 <strong>5</strong> 張個人檔案圖像, 及每次選擇其中一張以顯示為你的默認圖像。 你的圖像大小必須在 16x16 及 %sx%s pixels 之間。';
$string['setdefault'] = '設定默認';
$string['Title'] = '標題';
$string['imagetitle'] = '圖像標題';
$string['usenodefault'] = '使用沒有默認';
$string['usingnodefaultprofileicon'] = '現在使用沒有默認圖像';


?>
