<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = '個人資料';

$string['profile'] = '個人資料';
$string['myfiles'] = '我的文件';

$string['mandatory'] = '強制性';
$string['public'] = '公開';

$string['aboutdescription'] = '在這裡輸入您的真實姓名。如果你想顯示一個不同的名稱，請把該名稱作為您的首選名稱。';
$string['contactdescription'] = '除非您選擇將它公開,否則這些資料全部都是私人的。';
$string['messagingdescription'] = '這些資料將會保密,就像你的聯繫資料一樣。';
$string['viewmyprofile'] = '檢視我的個人資料';

// profile categories
$string['aboutme'] = '關於我';
$string['contact'] = '聯繫資料';
$string['messaging'] = '訊息';
$string['general'] = '一般';

// profile fields
$string['firstname'] = '名字';
$string['lastname'] = '姓氏';
$string['fullname'] = '全名';
$string['institution'] = '機構';
$string['studentid'] = '學生編號';
$string['preferredname'] = '首選名稱';
$string['introduction'] = '介紹';
$string['email'] = '電子郵件地址';
$string['officialwebsite'] = '官方網站地址';
$string['personalwebsite'] = '個人網站地址';
$string['blogaddress'] = '博客地址';
$string['address'] = '郵政地址';
$string['town'] = '城鎮';
$string['city'] = '城市/地區';
$string['country'] = '國家';
$string['homenumber'] = '住宅電話';
$string['businessnumber'] = '商務電話';
$string['mobilenumber'] = '手提電話';
$string['faxnumber'] = '傳真號碼';
$string['icqnumber'] = 'ICQ號碼';
$string['msnnumber'] = 'MSN聊天';
$string['aimscreenname'] = 'AIM名稱';
$string['yahoochat'] = 'Yahoo聊天';
$string['skypeusername'] = 'Skype用戶名稱';
$string['jabberusername'] = 'Jabber用戶名稱';
$string['occupation'] = '職業';
$string['industry'] = '行業';

// Field names for view user and search user display
$string['name'] = '姓名';
$string['principalemailaddress'] = '主要電子郵件';
$string['emailaddress'] = '替代電子郵件';

$string['saveprofile'] = '儲存個人資料';
$string['profilesaved'] = '個人資料儲存成功';
$string['profilefailedsaved'] = '個人資料儲存失敗';


$string['emailvalidation_subject'] = '電子郵件確認';
$string['emailvalidation_body'] = <<<EOF
%s 你好,

這個電郵地址 %s 已經加入了你在Mahara的帳戶。請訪問下面的連結來激活這個地址。

%s
EOF;

$string['validationemailwillbesent'] = '當您保存您的個人資料,驗證電子郵件將被發送';
$string['emailactivation'] = '電子郵件啟動';
$string['emailactivationsucceeded'] = '電子郵件啟動成功';
$string['emailactivationfailed'] = '電子郵件啟動失敗';
$string['unvalidatedemailalreadytaken'] = '您要驗證的電子郵件地址已被使用';

$string['emailingfailed'] = '已儲存個人資料，但郵件沒有發送到: %s';

$string['loseyourchanges'] = '失去你的更改？';

$string['editprofile']  = '修改個人資料';
$string['Title'] = '標題';

$string['Created'] = '已建立';
$string['Description'] = '描述';
$string['Download'] = '下載';
$string['lastmodified'] = '最後更新';
$string['Owner'] = '擁有者';
$string['Preview'] = '預覽';
$string['Size'] = '大小';
$string['Type'] = '類型';

?>