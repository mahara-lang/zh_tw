<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// General form strings
$string['add']     = '增加';
$string['cancel']  = '取消';
$string['delete']  = '刪除';
$string['edit']    = '編輯';
$string['editing'] = '編輯中';
$string['save']    = '儲存';
$string['submit']  = '提交';
$string['update']  = '更新';
$string['change']  = '更改';
$string['send']    = '傳送';
$string['go']      = '前往';
$string['default'] = '默認';
$string['upload']  = '上傳';
$string['complete']  = '完成';
$string['Failed']  = '失敗';
$string['loading'] = '載入中 ...';
$string['showtags'] = '顯示我的標籤';
$string['errorprocessingform'] = '提交此表格時出現錯誤。 請檢查已標記的範疇及再試一次。';
$string['description'] = '敘述';
$string['remove']  = '移除';

$string['no']     = '不是';
$string['yes']    = '是';
$string['none']   = '沒有';
$string['at'] = '在';
$string['From'] = '從';
$string['To'] = '至';
$string['All'] = '所有';

$string['next']      = '下一步';
$string['nextpage']  = '下一頁';
$string['previous']  = '上一步';
$string['prevpage']  = '上一頁';
$string['first']     = '第一步';
$string['firstpage'] = '首頁';
$string['last']      = '最後一步';
$string['lastpage']  = '最後頁';

$string['accept'] = '接受';
$string['reject'] = '拒絕';
$string['sendrequest'] = '傳送要求';
$string['reason'] = '原因';
$string['select'] = '選擇';

$string['tags'] = '標籤';
$string['tagsdesc'] = '輸入逗號以分隔此項目的標籤。';
$string['tagsdescprofile'] = '輸入逗號以分隔此項目的標籤。 項目帶有 \'個人檔案\'標籤顯示於側面欄.';
$string['youhavenottaggedanythingyet'] = '你還未在任何項目加上標籤。';

$string['selfsearch'] = '搜尋我的作品集';

// Quota strings
$string['quota'] = '限額';
$string['quotausage'] = '你已使用 <span id="quota_total">%s</span>中的<span id="quota_used">%s</span> 容量限額。';

$string['updatefailed'] = '更新失敗';

$string['strftimenotspecified']  = '沒有指明';

// profile sideblock strings
$string['invitedgroup'] = '被邀請的群組';
$string['invitedgroups'] = '被邀請的群組';
$string['logout'] = '登出';
$string['pendingfriend'] = '等候中的朋友';
$string['pendingfriends'] = '等候中的朋友';
$string['profile'] = '個人檔案';
$string['views'] = '檢視';

// Online users sideblock strings
$string['onlineusers'] = '線上用戶';
$string['lastminutes'] = '最後 %s 分鐘';

// Links and resources sideblock
$string['linksandresources'] = '連結及資源';

// auth
$string['accessforbiddentoadminsection'] = '你被禁止存取管理部分';
$string['accountdeleted'] = '對不起, 你的帳戶已被刪除';
$string['accountexpired'] = '對不起, 你的帳戶已過期';
$string['accountcreated'] = '%s: 新帳戶';
$string['accountcreatedtext'] = '親愛的 %s,

新帳戶已於 %s 為你建立。 你的資料如下:

用戶名稱: %s
密碼: %s

瀏覽 %s 開始吧!

 %s 網站管理員謹啟';
$string['accountcreatedchangepasswordtext'] = '親愛的 %s,

新帳戶已於 %s 為你建立。 你的資料如下:

用戶名稱: %s
密碼: %s

當你第一次登入, 你會被要求更改你的密碼。

瀏覽 %s 開始吧!

 %s 網站管理員謹啟';
$string['accountcreatedhtml'] = '<p>親愛的 %s</p>

<p>新帳戶已於<a href="%s">%s</a>為你建立。 你的資料如下: </p>

<ul>
    <li><strong>用戶名稱:</strong> %s</li>
    <li><strong>密碼:</strong> %s</li>
</ul>

<p>瀏覽 <a href="%s">%s</a> 開始吧!</p>

<p> %s 網站管理員謹啟</p>
';
$string['accountcreatedchangepasswordhtml'] = '<p>親愛的 %s</p>

<p>新帳戶已於<a href="%s">%s</a>為你建立。 你的資料如下: </p>

<ul>
    <li><strong>用戶名稱:</strong> %s</li>
    <li><strong>密碼:</strong> %s</li>
</ul>

<p>當你第一次登入, 你會被要求更改你的密碼。</p>

<p>瀏覽 <a href="%s">%s</a> 開始吧!</p>

<p> %s 網站管理員謹啟</p>
';
$string['accountexpirywarning'] = '帳戶過期警告';
$string['accountexpirywarningtext'] = '親愛的 %s,

你於 %s 的帳戶將會於 %s 後過期。

我們建議你使用輸出工具儲存你的作品集內容。 你可在使用說明中找到使用此功能的操作說明。

如你希望延長你的帳戶存取或有任何相關的問題, 請聯絡我們:

%s

%s 網站管理員謹啟';
$string['accountexpirywarninghtml'] = '<p>親愛的 %s,</p>
    
<p>你於 %s 的帳戶將會於 %s 後過期。</p>

<p>我們建議你使用輸出工具儲存你的作品集內容。 你可在使用說明中找到使用此功能的操作說明。</p>

<p>如你希望延長你的帳戶存取或有任何相關的問題, 請<a href="%s">聯絡我們</a>。</p>

<p>%s 網站管理員謹啟</p>';
$string['institutionexpirywarning'] = '機構會員身份過期警告';
$string['institutionexpirywarningtext'] = '親愛的 %s,

你在 %s 的 %s 會員身份將於 %s 後過期。

如你希望延長你的會員身份或有任何相關的問題, 請聯絡我們:

%s

%s 網站管理員謹啟';
$string['institutionexpirywarninghtml'] = '<p>親愛的 %s,</p>
    
<p>你在 %s 的 %s 會員身份將於 %s 後過期。</p>

<p>如你希望延長你的會員身份或有任何相關的問題, 請 <a href="%s">聯絡我們</a>。</p>

<p>%s 網站管理員謹啟</p>';
$string['accountinactive'] = '對不起, 你的帳戶現時無效';
$string['accountinactivewarning'] = '帳戶無效警告';
$string['accountinactivewarningtext'] = '親愛的 %s,

你於 %s 的帳戶將會於 %s 後無效。

當你的帳戶失效, 你將不可登入直至有管理人員恢復你的帳戶。

你可登入以避免帳戶失效。

%s 網站管理員謹啟';
$string['accountinactivewarninghtml'] = '<p>親愛的 %s,</p>

<p>你於 %s 的帳戶將會於 %s 後無效。</p>

<p>當你的帳戶失效, 你將不可登入直至有管理人員恢復你的帳戶。</p>

<p>你可登入以避免帳戶失效。</p>

<p>%s 網站管理員謹啟</p>';
$string['accountsuspended'] = '你於%s的帳戶已被中止。 你被中止的原因為:<blockquote>%s</blockquote>';
$string['youraccounthasbeensuspended'] = '你的帳戶已被中止';
$string['youraccounthasbeenunsuspended'] = '你的帳戶已被恢復';
$string['changepassword'] = '更改密碼';
$string['changepasswordinfo'] = '你需要更改密碼, 以前往下一步。';
$string['confirmpassword'] = '確認密碼';
$string['javascriptnotenabled'] = 'javascript。  你需要在登入Mahara前啟動javascript。';
$string['cookiesnotenabled'] = '你的瀏覽器沒有為此網站啟動cookies, 或封鎖此網站的cookies。  你需要在登入Mahara前啟動cookies。';
$string['institution'] = '機構';
$string['loggedoutok'] = '你已成功登出';
$string['login'] = '登入';
$string['loginfailed'] = '你沒有提供正確的憑證以登入。 請檢查你的用戶名稱及密碼是否正確。';
$string['loginto'] = ' 登入至 %s';
$string['newpassword'] = '新密碼';
$string['nosessionreload'] = '重新載入以登入';
$string['oldpassword'] = '現時密碼';
$string['password'] = '密碼';
$string['passworddescription'] = ' ';
$string['passwordhelp'] = '你存取系統時所使用的密碼';
$string['passwordnotchanged'] = '你沒有更改過你的密碼, 請選擇一個新的密碼';
$string['passwordsaved'] = '你的新密碼已儲存';
$string['passwordsdonotmatch'] = '密碼不相配';
$string['passwordtooeasy'] = '你的密碼太簡單了! 請選擇較複製的密碼。';
$string['register'] = '註冊';
$string['sessiontimedout'] = '你已超逾指定時間, 請輸入你的登入資料以繼續';
$string['sessiontimedoutpublic'] = '你已超逾指定時間。 你可 <a href="%s">登入</a> 以繼續瀏覽';
$string['sessiontimedoutreload'] = '你已超逾指定時間。 重新載入頁面以再次登入';
$string['username'] = '用戶名稱';
$string['preferredname'] = '暱稱';
$string['usernamedescription'] = ' ';
$string['usernamehelp'] = '你存取系統時所使用的用戶名稱。';
$string['youaremasqueradingas'] = '你正冒充為 %s。';
$string['yournewpassword'] = '你的新密碼';
$string['yournewpasswordagain'] = '你再次擁有新密碼';
$string['invalidsesskey'] = '無效的會議金匙';
$string['cannotremovedefaultemail'] = '你不可移除你的第一電郵地址';
$string['emailtoolong'] = '電郵地址不可多於 255 個字元';
$string['mustspecifyoldpassword'] = '你必須指明你現在的密碼';
$string['captchatitle'] = 'CAPTCHA 圖片';
$string['captchaimage'] = 'CAPTCHA 圖片';
$string['captchadescription'] = '輸入你在圖片中看到的字元。 字母沒有大小寫之分';
$string['captchaincorrect'] = '輸入圖片中顯示的字母樣式';
$string['Site'] = '網站';

// Misc. register stuff that could be used elsewhere
$string['emailaddress'] = '電郵地址';
$string['emailaddressdescription'] = ' ';
$string['firstname'] = '名字';
$string['firstnamedescription'] = ' ';
$string['lastname'] = '姓氏';
$string['lastnamedescription'] = ' ';
$string['studentid'] = '身份證明號碼';
$string['displayname'] = '顯示名稱';
$string['fullname'] = '全名';
$string['registerstep1description'] = '歡迎! 你必須先註冊以使用此網站。 而且你必須同意 <a href="terms.php">使用條款</a>。 我們在這裡收集的資料會根據 <a href="privacy.php">私隱聲明</a> 儲存。';
$string['registerstep3fieldsoptional'] = '<h3>選擇非必須的個人檔案圖片</h3><p>現在你已成功註冊 %s! 你現在可選擇顯示非必須的個人檔案圖像。</p>';
$string['registerstep3fieldsmandatory'] = '<h3>填寫強制個人檔案範疇</h3><p>以下的範疇為必須的項目。 你必須填寫才能完成註冊。</p>';
$string['registeringdisallowed'] = '對不起, 現在你不可註冊這個系統。';
$string['membershipexpiry'] = '會員身份過期';
$string['institutionfull'] = '你所選擇的機構不再接受註冊。';
$string['registrationnotallowed'] = '你所選擇的機構不允許自我註冊。';
$string['registrationcomplete'] = '謝謝你在 %s 的註冊';
$string['language'] = '語言';

// Forgot password
$string['cantchangepassword'] = '對不起, 你不可在此界面更改你的密碼 - 請使用你的機構的界面';
$string['forgotusernamepassword'] = '忘記你的用戶名稱或密碼?';
$string['forgotusernamepasswordtext'] = '<p>如你已忘記你的用戶名稱或密碼, 輸入在你個人檔案中的電郵地址, 我們會傳送訊息給你讓你更改新密碼。</p>
<p>如你知道你的用戶名稱但忘記你的密碼, 你也可輸入你的用戶名稱。</p>';
$string['lostusernamepassword'] = '遺失用戶名稱/密碼';
$string['emailaddressorusername'] = '電郵地址或用戶名稱';
$string['pwchangerequestsent'] = '你會立刻收到包含連結的電郵, 你可透過連結更改帳戶密碼。';
$string['forgotusernamepasswordemailsubject'] = '%s的用戶名稱/密碼資料';
$string['forgotusernamepasswordemailmessagetext'] = '親愛的 %s,

我們收到你在 %s 的帳戶所發出的用戶名稱及密碼要求。

你的用戶名稱是 %s 。

如你希望重設你的密碼, 請使用以下的連結:

%s

如你沒有要求密碼重設, 請忽略此電郵。

如有任何相關的問題, 請聯絡我們:

%s

%s 網站管理員謹啟';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>親愛的 %s,</p>

<p>我們收到你在 %s 的帳戶所發出的用戶名稱及密碼要求。</p>

<p>你的用戶名稱是 <strong>%s</strong>。</p>

<p>如你希望重設你的密碼, 請使用以下的連結:</p>

<p><a href="%s">%s</a></p>

<p>如你沒有要求密碼重設, 請忽略此電郵。</p>

<p>如有任何相關的問題, 請<a href="%s">聯絡我們</a>。</p>

<p>%s 網站管理員謹啟</p>';
$string['forgotpassemailsendunsuccessful'] = '對不起, 看來郵件並沒有傳送成功。 這是我們的錯誤, 請立刻重試';
$string['forgotpassnosuchemailaddressorusername'] = '你所輸入的電郵地址或用戶名稱跟網站任何一名用戶都不配合';
$string['forgotpasswordenternew'] = '請輸入你的新密碼以繼續';
$string['nosuchpasswordrequest'] = '沒有此密碼要求';
$string['passwordchangedok'] = '你的密碼曾成功更改';

// Reset password when moving from external to internal auth.
$string['noinstitutionsetpassemailsubject'] = '%s:  %s 的會員身份';
$string['noinstitutionsetpassemailmessagetext'] = '親愛的 %s,

你已經不是 %s 的會員了。
你可繼續在 %s 使用你現有的用戶名稱 %s, 但你必須為你的帳戶設立新密碼。

請使用以下的連結繼續重設的步驟。

%sforgotpass.php?key=%s

如有相關的問題, 請聯絡我們。

%scontact.php

 %s 網站管理員謹啟

%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>親愛的 %s,</p>

<p>你已經不是 %s 的會員了。</p>
<p>你可繼續在 %s 使用你現有的用戶名稱 %s, 但你必須為你的帳戶設立新密碼。</p>

<p>請使用以下的連結繼續重設的步驟。</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>

<p>如有相關的問題, 請<a href="%scontact.php">聯絡我們</a>。</p>

<p> %s 網站管理員謹啟</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';
$string['debugemail'] = '注意: 這郵件是為 %s <%s> 而設的, 但已傳送給你作為 "sendallemailto" 配置設定。';


// Expiry times
$string['noenddate'] = '沒有結束日期';
$string['day']       = '日';
$string['days']      = '日子';
$string['weeks']     = '星期';
$string['months']    = '月';
$string['years']     = '年';
// Boolean site option

// Site content pages
$string['sitecontentnotfound'] = '%s 文字無效';

// Contact us form
$string['name']                     = '名字';
$string['email']                    = '電郵';
$string['subject']                  = '主旨';
$string['message']                  = '訊息';
$string['messagesent']              = '你的訊息已傳送';
$string['nosendernamefound']        = '沒有提交收件者名稱';
$string['emailnotsent']             = '傳送聯絡電郵失敗。 錯誤訊息: "%s"';

// mahara.js
$string['namedfieldempty'] = '所需範疇 "%s" 是空白的';
$string['processing']     = '處理中';
$string['requiredfieldempty'] = '一個所需範疇是空白的';
$string['unknownerror']       = '出現不明錯誤 (0x20f91a0)';

// menu
$string['home']        = '主頁';
$string['myportfolio'] = '我的作品集';
$string['myviews']       = '我的選集';
$string['settings']    = '設定';
$string['myfriends']          = '我的朋友';
$string['findfriends']        = '搜尋朋友';
$string['groups']             = '群組';
$string['mygroups']           = '我的群組';
$string['findgroups']         = '搜尋群組';
$string['returntosite']       = '返回網站';
$string['siteadministration'] = '網站管理';
$string['useradministration'] = '用戶管理';
$string['viewmyprofilepage']  = '檢視個人檔案頁面';
$string['editmyprofilepage']  = '編輯個人檔案頁面';

$string['unreadmessages'] = '未讀取訊息';
$string['unreadmessage'] = '未讀取訊息';

$string['siteclosed'] = '由於資料庫升級的關係, 網站暫時關閉。  網站管理員可登入。';
$string['siteclosedlogindisabled'] = '由於資料庫升級的關係, 網站暫時關閉。  <a href="%s">現在執行升級。</a>';

// footer
$string['termsandconditions'] = '使用條款';
$string['privacystatement']   = '私隱聲明';
$string['about']              = '關於';
$string['contactus']          = '聯絡我們';

// my account
$string['account'] =  '我的帳戶';
$string['accountprefs'] = '喜好';
$string['preferences'] = '喜好';
$string['activityprefs'] = '活動喜好';
$string['changepassword'] = '更改密碼';
$string['notifications'] = '通知';
$string['institutionmembership'] = '機構會員身份';
$string['institutionmembershipdescription'] = '如你是某機構的會員, 它們會在這裡列出。 你也可向任何機構要求申請為會員, 及當任何機構邀請你加入時, 你可接受或拒絕邀請。';
$string['youareamemberof'] = '你是 %s 的會員';
$string['leaveinstitution'] = '離開機構';
$string['reallyleaveinstitution'] = '你確定你希望離開此機構?';
$string['youhaverequestedmembershipof'] = '你已要求成為 %s 的會員';
$string['cancelrequest'] = '取消要求';
$string['youhavebeeninvitedtojoin'] = '你被邀請加入 %s';
$string['confirminvitation'] = '確認邀請';
$string['joininstitution'] = '加入機構';
$string['decline'] = '拒絕';
$string['requestmembershipofaninstitution'] = '要求成為機構的會員';
$string['optionalinstitutionid'] = '機構身份認證碼 (非必須)';
$string['institutionmemberconfirmsubject'] = '機構會員身份確認';
$string['institutionmemberconfirmmessage'] = '你被加為 %s 的會員。';
$string['institutionmemberrejectsubject'] = '機構會員身份拒絕';
$string['institutionmemberrejectmessage'] = '你的 %s 會員身份要求被拒絕。';

$string['emailname'] = 'Mahara 系統'; // robot! 

$string['config'] = '設定';

$string['sendmessage'] = '傳送訊息';

$string['notinstallable'] = '非可安裝的!';
$string['installedplugins'] = '已安裝的插件';
$string['notinstalledplugins'] = '非已安裝插件';
$string['plugintype'] = '插件類型';

$string['settingssaved'] = '設定已儲存';
$string['settingssavefailed'] = '儲存設定失敗';

$string['width'] = '寬度';
$string['height'] = '高度';
$string['widthshort'] = 'w';
$string['heightshort'] = 'h';
$string['filter'] = '過濾器';
$string['expand'] = '展開';
$string['collapse'] = '摺疊';
$string['more...'] = '更多 ...';
$string['nohelpfound'] = '找不到此項目的說明';
$string['nohelpfoundpage'] = '找不到此頁面的說明';
$string['couldnotgethelp'] = '錯誤出現嘗試糾正說明頁面';
$string['profileimage'] = '個人檔案圖像';
$string['primaryemailinvalid'] = '你的第一電郵地址無效';
$string['addemail'] = '增加電郵地址';

// Search
$string['search'] = '搜尋';
$string['searchusers'] = '搜尋用戶';
$string['Query'] = '詢問';
$string['query'] = '詢問';
$string['querydescription'] = '用以搜尋的生字';
$string['result'] = '結果';
$string['results'] = '結果';
$string['Results'] = '結果';
$string['noresultsfound'] = '找不到任何結果';
$string['users'] = '用戶';

// artefact
$string['artefact'] = '作品';
$string['Artefact'] = '作品';
$string['Artefacts'] = '作品 ';
$string['artefactnotfound'] = '找不到身份 %s 的作品';
$string['artefactnotrendered'] = '沒有提供的作品';
$string['nodeletepermission'] = '你沒有被授權刪除此作品';
$string['noeditpermission'] = '你沒有授權編輯此作品';
$string['Permissions'] = '授權';
$string['republish'] = '發表';
$string['view'] = '選集';
$string['artefactnotpublishable'] = '作品 %s 沒有在選集 %s 中發表';

$string['belongingto'] = '屬於';
$string['allusers'] = '所有用戶';

// Upload manager
$string['quarantinedirname'] = '隔離';
$string['clammovedfile'] = '檔案已被移至隔離記錄.';
$string['clamdeletedfile'] = '檔案已被刪除';
$string['clamdeletedfilefailed'] = '檔案不可被刪除';
$string['clambroken'] = '你的網站管理員已於檔案上傳啟動病毒檢查, 但當中有一些錯誤的配置。 你的檔案上傳失敗。 你的網站管理人員已收到電郵通知讓他們可修正問題。 請稍後再嘗試上傳此檔案。 ';
$string['clamemailsubject'] = '%s :: Clam AV 通知';
$string['clamlost'] = 'Clam AV 被配置在檔案上傳中運作, 但支援Clam AV的路徑, %s, 是無效的。';
$string['clamfailed'] = 'Clam AV 運作失敗。  送回的錯誤訊息為 %s. 這是Clam的輸出:';
$string['clamunknownerror'] = '發生不明的clam錯誤。';
$string['image'] = '圖片';
$string['filenotimage'] = '你上傳的檔案是無效的圖片。 圖片必須是 PNG, JPEG 或 GIF 檔案。';
$string['uploadedfiletoobig'] = '此檔案太大。 請詢問你的管理員以了解更多資訊。';
$string['notphpuploadedfile'] = '檔案於上傳過程中遺失。 這是不應該發生的, 請聯絡你的網站管理員以了解更多資訊。';
$string['virusfounduser'] = '你所上傳的檔案, %s, 已被病毒檢查器掃瞄及確定為已中毒! 你的檔案上傳失敗。';
$string['fileunknowntype'] = '你上傳的檔案類型不明。 你的檔案可能已損壞, 或可能是配置的問題。 請聯絡你的管理員。';
$string['virusrepeatsubject'] = '警告: %s 是重複的病毒上傳者。';
$string['virusrepeatmessage'] = '用戶 %s 已上傳多個已被病毒檢查器掃瞄及確定為已中毒的檔案。';

$string['phpuploaderror'] = '上傳檔案時發生錯誤: %s (錯誤碼 %s)';
$string['phpuploaderror_1'] = '上傳的檔案超過php.ini檔案大小的最大限額。';
$string['phpuploaderror_2'] = '上傳的檔案超過HTML檔案大小的最大限額。';
$string['phpuploaderror_3'] = '上傳的檔案只上傳了一部分。';
$string['phpuploaderror_4'] = '沒有檔案已上傳。';
$string['phpuploaderror_6'] = '遺失暫時的文件夾。';
$string['phpuploaderror_7'] = '讀取檔案至磁碟失敗。';
$string['phpuploaderror_8'] = '上傳檔案已被擴展插件停止。';
$string['adminphpuploaderror'] = '上傳檔案錯誤可能被你的系統配置所引發。';

$string['youraccounthasbeensuspended'] = '你的帳戶已被中止';
$string['youraccounthasbeensuspendedtext2'] = '你在 %s 的帳戶被 %s 中止。'; // @todo: more info?
$string['youraccounthasbeensuspendedreasontext'] = "你在Y %s 的帳戶已被 %s 中止。 原因:\n\n%s";
$string['youraccounthasbeenunsuspended'] = '你的帳戶已恢復';
$string['youraccounthasbeenunsuspendedtext2'] = '你在 %s 的帳戶已恢復。 你可再次登入及使用網站。'; // can't provide a login link because we don't know how they log in - it might be by xmlrpc

// size of stuff
$string['sizemb'] = 'MB';
$string['sizekb'] = 'KB';
$string['sizegb'] = 'GB';
$string['sizeb'] = 'b';
$string['bytes'] = 'bytes';

// countries

$string['country.af'] = '阿富汗';
$string['country.ax'] = '奧蘭群島';
$string['country.al'] = '阿爾巴尼亞';
$string['country.dz'] = '阿爾及利亞';
$string['country.as'] = '美屬薩摩亞';
$string['country.ad'] = '安道爾共和國';
$string['country.ao'] = '安哥拉';
$string['country.ai'] = '安圭拉島';
$string['country.aq'] = '南極洲';
$string['country.ag'] = '安地卡及巴布達';
$string['country.ar'] = '阿根廷';
$string['country.am'] = '亞美尼亞';
$string['country.aw'] = '阿鲁巴';
$string['country.au'] = '澳大利亞';
$string['country.at'] = '奧地利';
$string['country.az'] = '亞塞拜然';
$string['country.bs'] = '巴哈馬';
$string['country.bh'] = '巴林';
$string['country.bd'] = '孟加拉共和國';
$string['country.bb'] = '巴貝多';
$string['country.by'] = '白俄羅斯';
$string['country.be'] = '比利時';
$string['country.bz'] = '貝里斯';
$string['country.bj'] = '貝南';
$string['country.bm'] = '百慕達';
$string['country.bt'] = '不丹';
$string['country.bo'] = '玻利維亞';
$string['country.ba'] = '波斯尼亞及黑塞哥維那';
$string['country.bw'] = '波紮那';
$string['country.bv'] = '鲍威特岛';
$string['country.br'] = '巴西';
$string['country.io'] = '英屬印度洋領地';
$string['country.bn'] = '汶萊';
$string['country.bg'] = '保加利亞';
$string['country.bf'] = '布基那法索國';
$string['country.bi'] = '蒲隆地';
$string['country.kh'] = '柬埔寨';
$string['country.cm'] = '喀麥隆';
$string['country.ca'] = '加拿大';
$string['country.cv'] = '維德角';
$string['country.ky'] = '開曼群島';
$string['country.cf'] = '中非';
$string['country.td'] = '查德';
$string['country.cl'] = '智利';
$string['country.cn'] = '中國';
$string['country.cx'] = '聖誕島';
$string['country.cc'] = '科科斯（基林）群岛';
$string['country.co'] = '哥倫比亞';
$string['country.km'] = '科摩洛';
$string['country.cg'] = '剛果';
$string['country.cd'] = '剛果民主共和國';
$string['country.ck'] = '庫克群島';
$string['country.cr'] = '哥斯大黎加';
$string['country.ci'] = '科特迪瓦';
$string['country.hr'] = '克羅埃西亞共和國';
$string['country.cu'] = '古巴';
$string['country.cy'] = '塞普勒斯';
$string['country.cz'] = '捷克共和國';
$string['country.dk'] = '丹麥';
$string['country.dj'] = '吉布地';
$string['country.dm'] = '多米尼克';
$string['country.do'] = '多明尼加共和國';
$string['country.ec'] = '厄瓜多爾';
$string['country.eg'] = '埃及';
$string['country.sv'] = '薩爾瓦多';
$string['country.gq'] = '赤道幾內亞';
$string['country.er'] = '厄立特里亞';
$string['country.ee'] = '愛沙尼亞';
$string['country.et'] = '衣索比亞';
$string['country.fk'] = '福克蘭群島 (馬爾維納斯)';
$string['country.fo'] = '法羅群島';
$string['country.fj'] = '斐濟';
$string['country.fi'] = '芬蘭';
$string['country.fr'] = '法國';
$string['country.gf'] = '法屬圭亞那';
$string['country.pf'] = '法屬玻里尼西亞';
$string['country.tf'] = '法屬南部領地';
$string['country.ga'] = '加彭';
$string['country.gm'] = '甘比亞';
$string['country.ge'] = '喬治亞';
$string['country.de'] = '德國';
$string['country.gh'] = '迦納';
$string['country.gi'] = '直布羅陀';
$string['country.gr'] = '希臘';
$string['country.gl'] = '格林蘭島';
$string['country.gd'] = '格瑞納達';
$string['country.gp'] = '哥德洛普島';
$string['country.gu'] = '關島';
$string['country.gt'] = '瓜地馬拉';
$string['country.gg'] = '格恩西島';
$string['country.gn'] = '幾內亞';
$string['country.gw'] = '幾內亞比紹(共和國)';
$string['country.gy'] = '蓋亞那';
$string['country.ht'] = '海地島';
$string['country.hm'] = '赫德島和麥克唐納群島';
$string['country.va'] = '梵蒂岡';
$string['country.hn'] = '宏都拉斯';
$string['country.hk'] = '香港';
$string['country.hu'] = '匈牙利';
$string['country.is'] = '冰島';
$string['country.in'] = '印度';
$string['country.id'] = '印尼';
$string['country.ir'] = '伊朗伊斯蘭共和國';
$string['country.iq'] = '伊拉克';
$string['country.ie'] = '愛爾蘭';
$string['country.im'] = '英國屬地曼島';
$string['country.il'] = '以色列';
$string['country.it'] = '意大利';
$string['country.jm'] = '牙買加';
$string['country.jp'] = '日本';
$string['country.je'] = '澤西島';
$string['country.jo'] = '約旦';
$string['country.kz'] = '哈薩克';
$string['country.ke'] = '肯亞';
$string['country.ki'] = '吉里巴斯共和國';
$string['country.kp'] = '北韓';
$string['country.kr'] = '南韓';
$string['country.kw'] = '科威特';
$string['country.kg'] = '吉爾吉斯共和國';
$string['country.la'] = '老撾';
$string['country.lv'] = '拉脫維亞';
$string['country.lb'] = '黎巴嫩';
$string['country.ls'] = '賴索托';
$string['country.lr'] = '賴比瑞亞';
$string['country.ly'] = '利比亞';
$string['country.li'] = '列支敦斯登';
$string['country.lt'] = '立陶宛';
$string['country.lu'] = '盧森堡';
$string['country.mo'] = '澳門';
$string['country.mk'] = '馬其頓共和國';
$string['country.mg'] = '馬達加斯加';
$string['country.mw'] = '馬拉威';
$string['country.my'] = '馬來西亞';
$string['country.mv'] = '馬爾代夫';
$string['country.ml'] = '馬利';
$string['country.mt'] = '馬爾他';
$string['country.mh'] = '馬紹爾群島';
$string['country.mq'] = '馬丁尼克島';
$string['country.mr'] = '茅利塔尼亞';
$string['country.mu'] = '模里西斯';
$string['country.yt'] = '馬約特';
$string['country.mx'] = '墨西哥';
$string['country.fm'] = '密克羅尼西亞聯邦';
$string['country.md'] = '摩爾多瓦';
$string['country.mc'] = '摩納哥';
$string['country.mn'] = '蒙古';
$string['country.ms'] = '蒙塞拉特島';
$string['country.ma'] = '摩洛哥';
$string['country.mz'] = '莫三比克';
$string['country.mm'] = '緬甸';
$string['country.na'] = '那米比亞';
$string['country.nr'] = '諾魯';
$string['country.np'] = '尼泊爾';
$string['country.nl'] = '荷蘭';
$string['country.an'] = '荷屬安的列斯';
$string['country.nc'] = '新加勒多尼亞';
$string['country.nz'] = '紐西蘭';
$string['country.ni'] = '尼加拉瓜';
$string['country.ne'] = '尼日';
$string['country.ng'] = '奈及利亞';
$string['country.nu'] = '紐埃';
$string['country.nf'] = '諾福克群島';
$string['country.mp'] = '北馬里亞納群島邦';
$string['country.no'] = '挪威';
$string['country.om'] = '阿曼';
$string['country.pk'] = '巴基斯坦';
$string['country.pw'] = '帛琉共和國';
$string['country.ps'] = '巴勒斯坦';
$string['country.pa'] = '巴拿馬';
$string['country.pg'] = '巴布新幾內亞';
$string['country.py'] = '巴拉圭';
$string['country.pe'] = '秘魯';
$string['country.ph'] = '菲律賓';
$string['country.pn'] = '皮特肯群島';
$string['country.pl'] = '波蘭';
$string['country.pt'] = '葡萄牙';
$string['country.pr'] = '波多黎各';
$string['country.qa'] = '卡達';
$string['country.re'] = '留尼旺島';
$string['country.ro'] = '羅馬尼亞';
$string['country.ru'] = '俄羅斯聯邦';
$string['country.rw'] = '盧旺達';
$string['country.sh'] = '聖赫勒拿島';
$string['country.kn'] = '聖克里斯多福與尼維斯聯邦';
$string['country.lc'] = '聖露西亞';
$string['country.pm'] = '聖皮耶與密克隆群島';
$string['country.vc'] = '聖文森及格瑞納丁';
$string['country.ws'] = '薩摩亞';
$string['country.sm'] = '聖馬利諾';
$string['country.st'] = '聖多美與普林希比共和國';
$string['country.sa'] = '沙地阿拉伯';
$string['country.sn'] = '塞內加爾';
$string['country.cs'] = '塞爾維亞和黑山';
$string['country.sc'] = '塞席爾群島';
$string['country.sl'] = '獅子山共和國';
$string['country.sg'] = '新加坡';
$string['country.sk'] = '斯洛伐克';
$string['country.si'] = '斯洛維尼亞共和國';
$string['country.sb'] = '所羅門群島';
$string['country.so'] = '索馬利亞';
$string['country.za'] = '南非';
$string['country.gs'] = '南喬治亞島與南桑威奇群島';
$string['country.es'] = '西班牙';
$string['country.lk'] = '斯里蘭卡';
$string['country.sd'] = '蘇丹';
$string['country.sr'] = '蘇利南共和國';
$string['country.sj'] = '斯瓦巴及尖棉';
$string['country.sz'] = '史瓦濟蘭';
$string['country.se'] = '瑞典';
$string['country.ch'] = '瑞士';
$string['country.sy'] = '阿拉伯叙利亞共和國';
$string['country.tw'] = '台灣';
$string['country.tj'] = '塔吉克';
$string['country.tz'] = '坦桑尼亞聯合共和國';
$string['country.th'] = '泰國';
$string['country.tl'] = '東帝汶民主共和國';
$string['country.tg'] = '多哥';
$string['country.tk'] = '托克勞';
$string['country.to'] = '東加';
$string['country.tt'] = '千里達托貝哥';
$string['country.tn'] = '突尼西亞';
$string['country.tr'] = '土耳其';
$string['country.tm'] = '土庫曼';
$string['country.tc'] = '特克斯和凱科斯群島';
$string['country.tv'] = '吐瓦魯';
$string['country.ug'] = '烏干達';
$string['country.ua'] = '烏克蘭';
$string['country.ae'] = '阿拉伯聯合大公國';
$string['country.gb'] = '英國';
$string['country.us'] = '美國';
$string['country.um'] = '美國本土外小島嶼';
$string['country.uy'] = '烏拉圭';
$string['country.uz'] = '烏茲別克斯坦';
$string['country.vu'] = '萬那杜';
$string['country.ve'] = '委內瑞拉';
$string['country.vn'] = '越南';
$string['country.vg'] = '英屬維京群島';
$string['country.vi'] = '美屬維京群島';
$string['country.wf'] = '瓦利斯和富圖納群島';
$string['country.eh'] = '西撒哈拉';
$string['country.ye'] = '葉門';
$string['country.zm'] = '尚比亞';
$string['country.zw'] = '辛巴威';

// general stuff that doesn't really fit anywhere else
$string['system'] = '系統';
$string['done'] = '完成';
$string['back'] = '返回';
$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
$string['formatpostbbcode'] = '你可使用BB代碼更改文章格式。 %s學習更多%s';

// import related strings (maybe separated later)
$string['importedfrom'] = '從 %s 匯入';
$string['incomingfolderdesc'] = '從其他網絡主機匯入的檔案';
$string['remotehost'] = '遞控主機 %s';

$string['Copyof'] = ' %s 的複製檔案';

// Profie views
$string['loggedinusersonly'] = '只限已登入用戶';
$string['allowpublicaccess'] = '允許公開(非已登入) 存取';
$string['thisistheprofilepagefor'] = '這是 %s 的個人檔案頁面';

$string['pleasedonotreplytothismessage'] = "請不要回覆此訊息。";
?>