<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['allmydata'] = '我所有的資料';
$string['chooseanexportformat'] = '請選擇一種匯出格式';
$string['clicktopreview'] = '點擊預覽';
$string['creatingzipfile'] = '正在產生壓縮檔案';
$string['Done'] = '已完成';
$string['Export']     = '匯出';
$string['exportgeneratedsuccessfully'] = '已成功產生匯出檔。%s 點擊這裡下載 %s';
$string['exportgeneratedsuccessfullyjs'] = '已成功產生匯出檔。%s 繼續 %s';
$string['exportingartefactplugindata'] = '正在匯出作品插件資料';
$string['exportingartefacts'] = '正在匯出作品';
$string['exportingartefactsprogress'] = '正在匯出作品: %s/%s';
$string['exportingfooter'] = '正在匯出頁尾';
$string['exportingviews'] = '正在匯出選集';
$string['exportingviewsprogress'] = '正在匯出選集: %s/%s';
$string['exportpagedescription'] = '在這裡你可以匯出你的歷程檔案。本工具可以匯出你歷程檔案的資料和選集，但不能匯出你在網站的設定值。';
$string['exportyourportfolio'] = '匯出你的歷程檔案';
$string['generateexport'] = '產生匯出檔';
$string['noexportpluginsenabled'] = '管理員並沒有啟用任何匯出插件，所以你不能使用這個功能。';
$string['justsomeviews'] = '我一部份的選集';
$string['pleasewaitwhileyourexportisbeinggenerated'] = '正在產生你的匯出檔，請稍候...';
$string['setupcomplete'] = '已完成設定';
$string['Starting'] = '開始中';
$string['unabletoexportportfoliousingoptions'] = '使用你選擇的選項不能匯出一個歷程檔案';
$string['unabletogenerateexport'] = '未能產生匯出檔';
$string['viewstoexport'] = '將匯出的選集';
$string['whatdoyouwanttoexport'] = '你想匯出甚麼？';
$string['writingfiles'] = '正在寫入檔案';
$string['youarehere'] = '你在此';
$string['youmustselectatleastoneviewtoexport'] = '你心需最少選擇一個選集來匯出';
$string['zipnotinstalled'] = '你的系統不能使用zip指令。請安裝zip來啟用這個功能';

?>
