<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage notification-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['typemaharamessage'] = '系統訊息';
$string['typeusermessage'] = '來自其他用戶的留言';
$string['typefeedback'] = '回應';
$string['typewatchlist'] = '追踪監視表';
$string['typeviewaccess'] = '新選集的存取';
$string['typecontactus'] = '聯絡我們';
$string['typeobjectionable'] = '令人反感的內容';
$string['typevirusrepeat'] ='重複病毒上傳';
$string['typevirusrelease'] = '解放病毒標示';
$string['typeadminmessages'] = '管理信息';
$string['typeinstitutionmessage'] = '機構信息';
$string['typegroupmessage'] = '群組信息';

$string['type'] = '活動類型';
$string['attime'] = '在';
$string['prefsdescr'] = '如果您選擇任何一個電子郵件選項，活動記錄仍會有通知，但它們將被自動標記為已讀。';

$string['subject'] = '話題';
$string['date'] = '日期';
$string['read'] = '已讀';
$string['unread'] = '未讀';

$string['markasread'] = '標記為已讀';
$string['selectall'] = '選取所有';
$string['recurseall'] = '循還所有';
$string['alltypes'] = '所有類型';

$string['markedasread'] = '標記您的通知為已讀';
$string['failedtomarkasread'] = '無法標記您的通知為已讀';

$string['deletednotifications'] = '已刪除 %s 通知';
$string['failedtodeletenotifications'] = '無法刪除您的通知';

$string['stopmonitoring'] = '停止監視';
$string['viewsandartefacts'] = '選集和作品';
$string['views'] = '選集';
$string['artefacts'] = '作品';
$string['groups'] = '群組';
$string['monitored'] = '已監視';
$string['stopmonitoring'] = '停止監視';

$string['stopmonitoringsuccess'] = '成功停止監視';
$string['stopmonitoringfailed'] = '未能停止監視';

$string['newwatchlistmessage'] = '您的監視表內的新活動';
$string['newwatchlistmessageview'] = '%s 已更改選集 "%s"';

$string['newviewsubject'] = '已創建新選集';
$string['newviewmessage'] = '%s 創建了一個新選集 "%s"';

$string['newcontactusfrom'] = '新的連絡我們自';
$string['newcontactus'] = '新的連絡我們';
$string['newfeedbackonview'] = '新的選集回應';
$string['newfeedbackonartefact'] = '新的作品回應';

$string['newviewaccessmessage'] = '您已被添加到選集的訪問列表,名稱是: "%s" 由 %s';
$string['newviewaccesssubject'] = '進入新選集';

$string['viewmodified'] = '已更改他們的選集';
$string['ongroup'] = '在群組';
$string['ownedby'] = '擁有者';

$string['objectionablecontentview'] = '%s 報告令人反感的內容 "%s"';
$string['objectionablecontentartefact'] = '組件的反感內容 "%s" 是由 %s 所報告';

$string['newgroupmembersubj'] = '%s 現在已成為會員!';
$string['removedgroupmembersubj'] = '%s 已不再是會員';

$string['addtowatchlist'] = '添加到監視表';
$string['removefromwatchlist'] = '從監視表刪除';

$string['missingparam'] = '所需的參數 %s 是空的，活動類別是 %s';

$string['institutionrequestsubject'] = '%s 要求成為 %s的會員';
$string['institutionrequestmessage'] = '您可以於機構成員網頁添加用戶到機構:';

$string['institutioninvitesubject'] = '您已被邀請加入%s';
$string['institutioninvitemessage'] = '您可以您的機構設置頁確認您的會員身份:';

?>