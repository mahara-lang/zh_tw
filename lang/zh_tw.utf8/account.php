<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage core
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['changepassworddesc'] = '如果您想更改您的密碼，請在此輸入詳情';
$string['changepasswordotherinterface'] = '您可以<a href="%s">通過不同的界面</a>更改密碼</a>';
$string['oldpasswordincorrect'] = '這不是你當前的密碼';

$string['changeusernameheading'] = '更改用戶名';
$string['changeusername'] = '新用戶名';
$string['changeusernamedesc'] = '您登錄到 %s使用的用戶名。用戶名是3至30字元長/(不包括空格/)，並可能包含字母，數字和最常見的符號';

$string['accountoptionsdesc'] = '您可以在此設置一般帳戶選擇';
$string['friendsnobody'] = '沒有人加入我為好友';
$string['friendsauth'] = '加入新朋友前要得到我的授權';
$string['friendsauto'] = '新朋友可自動得到我的授權';
$string['friendsdescr'] = '好友控制';
$string['updatedfriendcontrolsetting'] = '更新好友控制';

$string['wysiwygdescr'] = 'HTML編輯器';
$string['on'] = '開';
$string['off'] = '關';

$string['messagesdescr'] = '來自其他用戶的郵件';
$string['messagesnobody'] = '不要允許任何人向我發送郵件';
$string['messagesfriends'] = '讓好友列表中的朋友向我發送郵件';
$string['messagesallow'] = '允許任何人向我發送郵件';

$string['language'] = '語言';

$string['showviewcolumns'] = '在編輯選集時顯示添加和刪除列的控制';

$string['tagssideblockmaxtags'] = '顯示在標籤雲的標籤數量';
$string['tagssideblockmaxtagsdescription'] = '顯示在用戶標籤雲的預設標籤數量';

$string['prefssaved']  = '使用偏好已保存';
$string['prefsnotsaved'] = '無法保存您的使用偏好!';

$string['deleteaccount']  = '刪除賬戶';
$string['deleteaccountdescription']  = '如果你刪除你的賬戶，你的個人資料和選集將不能夠向其他用戶顯示。任何你在論壇寫過的帖子還會顯示出來，但作者的名稱則不會再顯示。';
$string['accountdeleted']  = '你的賑戶已被刪除。';
?>