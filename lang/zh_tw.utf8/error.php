<?php
/**
 * Mahara: Electronic portfolio，weblog，resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation，either version 3 of the License，or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not，see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// @todo<nigel>: most likely need much better descriptions here for these environment issues
$string['phpversion'] = 'Mahara不可在 PHP < %s 運作。 請給你的 PHP 版本升級，或將Mahara移至另一個主機。';
$string['jsonextensionnotloaded'] = '你的伺服器配置沒有包含JSON擴展插件。 Mahara需使用此程式從瀏覽器傳送資料或傳送資料至瀏覽器。 請確認它已載入為php.ini，或安裝它。';
$string['pgsqldbextensionnotloaded'] = '你的伺服器配置沒有包含pgsql擴展插件。 Mahara需使用此程式儲存資料至資料庫。  請確認它已載入為php.ini，或安裝它。';
$string['mysqldbextensionnotloaded'] = '你的伺服器配置沒有包含mysql擴展插件。 Mahara需使用此程式儲存資料至資料庫。  請確認它已載入為php.ini，或安裝它。';
$string['unknowndbtype'] = '你的伺服器配置推薦不明的資料庫類型。 有效數值為"postgres8" 及 "mysql5"。 請於config.php更改資料庫類型設定。';
$string['xmlextensionnotloaded'] = '你的伺服器配置沒有包含 %s 擴展部分插件。 Mahara需使用此程式分析來自不同來源的資料。 請確認它已載入為php.ini，或安裝它。';
$string['gdextensionnotloaded'] = '你的伺服器配置沒有包含 gd 擴展插件。 Mahara需使用此程式重設上傳圖像的大小或其他有關圖像的操作。 請確認它已載入為php.ini，或安裝它。';
$string['gdfreetypenotloaded'] = '你的gd 擴展插件的系統配置沒有包含Freetype支援。 Mahara需使用此程式建設CAPTCHA圖像。 請確認gd已跟它配置。';
$string['sessionextensionnotloaded'] = '你的伺服器配置沒有包含會議擴展插件 (session extension)。 Mahara需使用此程式支援用戶登入。 請確認它已載入為php.ini，或安裝它。';
$string['curllibrarynotinstalled'] = '你的伺服器配置沒有包含curl擴展插件。 Mahara需使用此程式跟Moodle整合及擷取外部消息來源。 請確認它已載入為php.ini，或安裝它。';
$string['registerglobals'] = '你有不安全的PHP設定，register_globals已啟動。 Mahara正嘗試處理它，但你應該解決它';
$string['magicquotesgpc'] = '你有不安全的PHP設定，magic_quotes_gpc已啟動。 Mahara正嘗試處理它，但你應該解決它';
$string['magicquotesruntime'] = '你有不安全的PHP設定，magic_quotes_runtime已啟動。 Mahara正嘗試處理它，但你應該解決它';
$string['magicquotessybase'] = '你有不安全的PHP設定，magic_quotes_sybase已啟動. Mahara正嘗試處理它，但你應該解決它';

$string['safemodeon'] = '你的伺服器正以安全模式運作。 Mahara並不支援安全模式。 你必須於php.ini檔案或你在此網站的apache配置關掉它。

如你使用共同主機，除了詢問你的主機提供者，你或許不能將安全模式關掉。 也許你可以考慮移至另一個主機。';
$string['datarootinsidedocroot'] = '你已將你的資料目錄設定在文件目錄中。 這是很大的安全問題，這樣任何人也可以直接要求會議(session)資料 (以搶奪別人的會議)，或其他人上傳而他們不能存取的檔案。 請將資料目錄配置於文件目錄之外。';
$string['datarootnotwritable'] = '你的已定義資料目錄 (data root directory)，%s，無法被寫入. 這表示需要上傳的會議(session)資料，用戶檔案或其他資料均不能儲存在你的伺服器。 如目錄並不存在，請建立目錄，或將目錄的擁有權交給網站伺服器用戶。 ';
$string['couldnotmakedatadirectories'] = '由於某些原因，某些核心資料目錄無法建立。 這是不可能發生的，因為Mahara在之前已發現資料目錄(data root directory)是可寫入的。 請檢查資料目錄的許可。';

$string['dbconnfailed'] = 'Mahara不能連結至應用程式資料庫。

 * If 你正使用Mahara，請稍等及重試
 * If 你是管理員，請檢查你的資料庫設定及確認你的資料庫是有效的

收到的錯誤為:
';
$string['dbversioncheckfailed'] = '你的資料庫伺服器版本太舊，因此Mahara不能成功運作。 你的伺服器是 %s %s，但Mahara所需的版本至少是 %s。';

// general exception error messages
$string['blocktypenametaken'] = "組件類型 %s 已被另一個插件(%s) 佔用。";
$string['artefacttypenametaken'] = "作品類型 %s 已被另一個插件(%s) 佔用。";
$string['classmissing'] = "沒有插件%s中類型%s的級別";
$string['artefacttypeclassmissing'] = "作品類型必須實施級別。 沒有 %s";
$string['artefactpluginmethodmissing'] =  "作品插件 %s 必須實施 %s 但並沒有";
$string['blocktypelibmissing'] = '沒有於作品插件%s中組件 %s 的lib.php ';
$string['blocktypemissingconfigform'] = '作品類型 %s 必須實施instance_config_form';
$string['versionphpmissing'] = '插件 %s %s 沒有 version.php!';
$string['blocktypeprovidedbyartefactnotinstallable'] = '這將會安裝為作品插件 %s 安裝的其中一部分。 ';
$string['blockconfigdatacalledfromset'] = '配置資料必須直接設定，使用PluginBlocktype::instance_config_save 作為替代';
$string['invaliddirection'] = '無效的指向 %s';
$string['onlyoneprofileviewallowed'] = '你只可有一個個人檔案選集。';

// if you change these next two ，be sure to change them in libroot/errors.php
// as they are duplicated there，in the case that get_string was not available.
$string['unrecoverableerror'] = '發生無法復原的錯誤。 這表示你遇到系統的故障。';
$string['unrecoverableerrortitle'] = '%s - 無法載入網站';
$string['parameterexception'] = '沒有所需參數';

$string['notfound'] = '找不到';
$string['notfoundexception'] = '找不到你搜尋的頁面';

$string['accessdenied'] = '拒絕存取';
$string['accessdeniedexception'] =  '你沒有此頁面的存取權限';

$string['viewnotfoundexceptiontitle'] = '找不到選集';
$string['viewnotfoundexceptionmessage'] = '你嘗試存取的選集並不存在!';
$string['viewnotfound'] = '找不到帳號 %s 的選集';

$string['artefactnotfoundmaybedeleted'] = "找不到帳號 %s 的作品(也許已被刪除?)";
$string['artefactnotfound'] = '找不到帳號 %s 的作品';
$string['notartefactowner'] = '你並不擁有此作品';

$string['blockinstancednotfound'] = '找不到帳號 %s 的組件例子';
$string['interactioninstancenotfound'] = '找不到帳號 %s 的活動例子';

$string['invalidviewaction'] = '無效選集管制操作: %s';

$string['missingparamblocktype'] = '先嘗試選擇及新增組件類型';
$string['missingparamcolumn'] = '沒有指明欄位';
$string['missingparamorder'] = '沒有指明次序';
$string['missingparamid'] = '沒有帳號';
?>
