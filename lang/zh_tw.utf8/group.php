<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// my groups
$string['groupname'] = '群組名稱';
$string['creategroup'] = '建立群組';
$string['groupmemberrequests'] = '等候中的會員身份要求';
$string['sendinvitation'] = '傳送邀請';
$string['invitetogroupsubject'] = '你被邀請加入某群組';
$string['invitetogroupmessage'] = '%s 已邀請你加入群組, \'%s\'.  點擊以下的連結以了解更多資訊。';
$string['inviteuserfailed'] = '邀請用戶失敗';
$string['userinvited'] = '邀請已傳送';
$string['addedtogroupsubject'] = '你已被加入某群組';
$string['addedtogroupmessage'] = '%s 已將你加入群組, \'%s\'.  點擊以下的連結以觀看群組';
$string['adduserfailed'] = '加入用戶失敗';
$string['useradded'] = '用戶已加入';
$string['editgroup'] = '編輯群組';
$string['savegroup'] = '儲存群組';
$string['groupsaved'] = '群組儲存成功';
$string['invalidgroup'] = '此群組不存在';
$string['canteditdontown'] = '由於你不是擁有者, 你不可編輯此群組';
$string['groupdescription'] = '群組敘述';
$string['membershiptype'] = '群組會員類型';
$string['membershiptype.controlled'] = '受管理會員';
$string['membershiptype.invite']     = '只限邀請';
$string['membershiptype.request']    = '要求會員身份';
$string['membershiptype.open']       = '公開會員身份';
$string['pendingmembers']            = '等候中的用戶';
$string['reason']                    = '原因';
$string['approve']                   = '批准';
$string['reject']                    = '拒絕';
$string['groupalreadyexists'] = '此名稱的群組已存在';
$string['Created'] = '已建立';
$string['groupadmins'] = '群組管理人員';
$string['Admin'] = '管理';
$string['grouptype'] = '群組類型';
$string['publiclyviewablegroup'] = '可公開瀏覽的群組?';
$string['publiclyviewablegroupdescription'] = '允許所有人 (包括非網站會員) 觀看此群組, 包括討論區?';
$string['usersautoadded'] = '自動加入用戶?';
$string['usersautoaddeddescription'] = '自動將所有新用戶加入此群組?';

$string['hasrequestedmembership'] = '已要求成為此群組會員';
$string['hasbeeninvitedtojoin'] = '已被邀請加入此群組';
$string['groupinvitesfrom'] = '被邀請加入:';
$string['requestedmembershipin'] = '要求成為會員:';

// Used to refer to all the members of a group - NOT a "member" group role!
$string['member'] = '會員';
$string['members'] = '會員';
$string['Members'] = '會員';

$string['memberrequests'] = '會員身份要求';
$string['declinerequest'] = '拒絕要求';
$string['submittedviews'] = '已提交選集';
$string['releaseview'] = '發回選集';
$string['invite'] = '邀請';
$string['remove'] = '移除';
$string['updatemembership'] = '更新會員身份';
$string['memberchangefailed'] = '更新某些會員資料失敗';
$string['memberchangesuccess'] = '會員身份狀態更改成功';
$string['viewreleasedsubject'] = '你的選集已發回';
$string['viewreleasedmessage'] = '你提交至群組 %s 的選集已從 %s 發回給你';
$string['viewreleasedsuccess'] = '發回選集成功';
$string['groupmembershipchangesubject'] = '群組會員身份: %s';
$string['groupmembershipchangedmessagetutor'] = '你被升任為此群組的導師';
$string['groupmembershipchangedmessagemember'] = '你已從群組導師降級';
$string['groupmembershipchangedmessageremove'] = '你已從群組中被移除';
$string['groupmembershipchangedmessagedeclinerequest'] = '你加入此群組的要求已被拒絕';
$string['groupmembershipchangedmessageaddedtutor'] = '你已被加入為此群組的導師';
$string['groupmembershipchangedmessageaddedmember'] = '你已被加入為此群組的會員';
$string['leavegroup'] = '離開此群組';
$string['joingroup'] = '加入此群組';
$string['requestjoingroup'] = '要求加入此群組';
$string['grouphaveinvite'] = '你已被邀請加入此群組';
$string['grouphaveinvitewithrole'] = '你已被邀請使用指定角色加入此群組';
$string['groupnotinvited'] = '你沒有被邀請加入此群組';
$string['groupinviteaccepted'] = '接受邀請成功! 你現在已是群組會員';
$string['groupinvitedeclined'] = '拒絕邀請成功!';
$string['acceptinvitegroup'] = '接受';
$string['declineinvitegroup'] = '拒絕';
$string['leftgroup'] = '你現在已離開此群組';
$string['leftgroupfailed'] = '離開群組失敗';
$string['couldnotleavegroup'] = '你不可離開此群組';
$string['joinedgroup'] = '你現在已是群組會員';
$string['couldnotjoingroup'] = '你不可加入此群組';
$string['grouprequestsent'] = '已傳送群組會員身份要求';
$string['couldnotrequestgroup'] = '不能傳送群組會員身份要求';
$string['cannotrequestjoingroup'] ='你不可要求加入此群組';
$string['groupjointypeopen'] = '此群組的會員身份是公開的。 儘管加入吧!';
$string['groupjointypecontrolled'] = '此群組的會員身份是受管理的。 你不可加入此群組。';
$string['groupjointypeinvite'] = '此群組只限邀請會員加入';
$string['groupjointyperequest'] = '此群組只限會員要求加入。';
$string['grouprequestsubject'] = '新的會員身份要求';
$string['grouprequestmessage'] = '%s 希望加入你的群組 %s';
$string['grouprequestmessagereason'] = "%s 希望加入你的群組 %s. 他們希望加入的原因是:\n\n%s";
$string['cantdeletegroup'] = '你不可刪除此群組';
$string['groupconfirmdelete'] = '你確定你希望刪除此群組?';
$string['groupconfirmdeletehasviews'] = '你確定你希望刪除此群組? 你的某些選集使用此群組作存取控制, 移除此群組會令群組會員失去那些選集的存取資格。';
$string['deletegroup'] = '刪除群組成功';
$string['allmygroups'] = '我的所有群組';
$string['groupsimin']  = '我所屬的群組';
$string['groupsiown']  = '我所擁有的群組';
$string['groupsiminvitedto'] = '我被邀請加入的群組';
$string['groupsiwanttojoin'] = '我希望加入的群組';
$string['therearependingrequests'] = '此群組有 %s 等候中的會員身份要求。';
$string['thereispendingrequest'] = '此群組有1個等候中的會員身份要求。';
$string['requestedtojoin'] = '你已要求加入此群組';
$string['groupnotfound'] = '找不到身份認證碼為 %s 的群組';
$string['groupconfirmleave'] = '你確定你希望離開此群組?';
$string['groupconfirmleavehasviews'] = '你確定你希望離開此群組? 你的某些選集使用此群組作存取控制, 離開此群組會令群組會員失去那些選集的存取資格。';
$string['cantleavegroup'] = '你不可離開此群組';
$string['usercantleavegroup'] = '此用戶不可離開此群組';
$string['usercannotchangetothisrole'] = '用戶不可更改至此角色';
$string['leavespecifiedgroup'] = '離開群組 \'%s\'';
$string['memberslist'] = '會員: ';
$string['nogroups'] = '無群組';
$string['deletespecifiedgroup'] = '刪除群組 \'%s\'';
$string['requestjoinspecifiedgroup'] = '要求加入群組 \'%s\'';
$string['youaregroupmember'] = '你是此群組的會員';
$string['youowngroup'] = '你擁有此群組';
$string['groupsnotin'] = '我沒有加入的群組';
$string['allgroups'] = '所有群組';
$string['allgroupmembers'] = '所有群組會員';
$string['trysearchingforgroups'] = '嘗試 %s搜尋群組%s 以加入!';
$string['nogroupsfound'] = '找不到任何群組 :(';
$string['group'] = '群組';
$string['Group'] = '群組';
$string['groups'] = '群組';
$string['notamember'] = '你不是此群組的會員';
$string['notmembermayjoin'] = '你必須加入群組 \'%s\' 以觀看此頁面。';

// friendslist
$string['reasonoptional'] = '原因 (非必須)';
$string['request'] = '要求';

$string['friendformaddsuccess'] = '已加入 %s 至你的朋友列表';
$string['friendformremovesuccess'] = '已從你的朋友列表中從除 %s ';
$string['friendformrequestsuccess'] = '已傳送朋友邀請至 %s';
$string['friendformacceptsuccess'] = '已接受朋友邀請';
$string['friendformrejectsuccess'] = '已拒絕朋友邀請';

$string['addtofriendslist'] = '加至朋友';
$string['requestfriendship'] = '要求成為朋友';

$string['addedtofriendslistsubject'] = '新朋友';
$string['addedtofriendslistmessage'] = '%s 已將你加為朋友! 這代表 %s 也在你的朋友列表中。 '
    . ' 點擊以下的連結以觀看他們的個人檔案頁面';

$string['requestedfriendlistsubject'] = '新朋友要求';
$string['requestedfriendlistmessage'] = '%s 要求你將他們加為朋友。  '
    .' 你可使用以下的連結或朋友列表頁面';

$string['requestedfriendlistmessagereason'] = '%s 要求你將他們加為朋友。'
    . ' 你可使用以下的連結或朋友列表頁面。'
    . ' 他們的原因是:
    ';

$string['removefromfriendslist'] = '從朋友中移除';
$string['removefromfriends'] = '從朋友中移除 %s ';
$string['confirmremovefriend'] = '你確定你希望從朋友列表中移除此用戶?';
$string['removedfromfriendslistsubject'] = '從朋友列表中移除';
$string['removedfromfriendslistmessage'] = '%s 已將你從朋友列表中移除。';
$string['removedfromfriendslistmessagereason'] = '%s 已將你從他們的朋友列表中移除。  他們的原因是: ';
$string['cantremovefriend'] = '你不可將此用戶從你的朋友列表中移除';

$string['friendshipalreadyrequested'] = '你被要求加至 %s 的朋友列表';
$string['friendshipalreadyrequestedowner'] = '%s 要求加至你的朋友列表';
$string['rejectfriendshipreason'] = '拒絕要求的原因';

$string['friendrequestacceptedsubject'] = '已接受朋友要求';
$string['friendrequestacceptedmessage'] = '%s 已接受你的朋友要求,而且他們已加至你的朋友列表'; 
$string['friendrequestrejectedsubject'] = '已拒絕朋友要求';
$string['friendrequestrejectedmessage'] = '%s 已拒絕你的朋友要求。';
$string['friendrequestrejectedmessagereason'] = '%s 已拒絕你的朋友要求。他們的原因是: ';

$string['allfriends']     = '所有朋友';
$string['currentfriends'] = '現有的朋友';
$string['pendingfriends'] = '等候中的朋友';
$string['backtofriendslist'] = '回到朋友列表';
$string['findnewfriends'] = '搜尋新朋友';
$string['Views']          = '選集';
$string['Files']          = '檔案';
$string['seeallviews']    = '觀看所有 %s 的選集...';
$string['noviewstosee']   = '沒有你可以觀看的 :(';
$string['whymakemeyourfriend'] = '因此你必須讓我成為你的朋友:';
$string['approverequest'] = '批准要求!';
$string['denyrequest']    = '拒絕要求';
$string['pending']        = '等候中';
$string['trysearchingforfriends'] = '嘗試 %s搜尋新朋友%s 以擴大你的網絡!';
$string['nobodyawaitsfriendapproval'] = '沒有人在等候你批准他們成為你的朋友';
$string['sendfriendrequest'] = '傳送朋友要求!';
$string['addtomyfriends'] = '加至我的朋友!';
$string['friendshiprequested'] = '已要求成為朋友!';
$string['existingfriend'] = '現有的朋友';
$string['nosearchresultsfound'] = '找不到搜尋的結果 :(';
$string['friend'] = '朋友';
$string['friends'] = '朋友';
$string['user'] = '用戶';
$string['users'] = '用戶';
$string['Friends'] = '朋友';

$string['friendlistfailure'] = '修改朋友列表失敗';
$string['userdoesntwantfriends'] = '此用戶不希望有新朋友';
$string['cannotrequestfriendshipwithself'] = '你不可要求成為自己的朋友';
$string['cantrequestfriendship'] = '你不可要求成為此用戶的朋友';

// Messaging between users
$string['messagebody'] = '傳送訊息'; // wtf
$string['sendmessage'] = '傳送訊息';
$string['messagesent'] = '訊息已傳送!';
$string['messagenotsent'] = '傳送訊息失敗';
$string['newusermessage'] = ' %s 傳送給你的新訊息';
$string['newusermessageemailbody'] = '%s 傳送一則訊息給你。  瀏覽以下連結以觀看訊息

%s';
$string['sendmessageto'] = '傳送訊息給 %s';
$string['viewmessage'] = '檢視訊息';
$string['Reply'] = '回覆';

$string['denyfriendrequest'] = '拒絕朋友要求';
$string['sendfriendshiprequest'] = '傳送朋友要求給 %s ';
$string['cantdenyrequest'] = '朋友要求無效';
$string['cantrequestfrienship'] = '你不可要求成為此用戶的朋友';
$string['cantmessageuser'] = '你不可傳送訊息給此用戶';
$string['requestedfriendship'] = '已要求成為朋友';
$string['notinanygroups'] = '不在任何群組';
$string['addusertogroup'] = '新增至 ';
$string['inviteusertojoingroup'] = '邀請至 ';
$string['invitemembertogroup'] = '邀請 %s 加入 \'%s\'';
$string['cannotinvitetogroup'] = '你不可邀請此用戶至此群組';
$string['useralreadyinvitedtogroup'] = '此用戶已被邀請, 或已是此群組的會員。';
$string['removefriend'] = '移除朋友';
$string['denyfriendrequestlower'] = '拒絕朋友要求';

// Group interactions (activities)
$string['groupinteractions'] = '群組活動';
$string['nointeractions'] = '此群組沒有活動';
$string['notallowedtoeditinteractions'] = '你不可在此群組增加或編輯活動';
$string['notallowedtodeleteinteractions'] = '你不可刪除此群組的活動';
$string['interactionsaved'] = '%s 儲存成功';
$string['deleteinteraction'] = '刪除 %s \'%s\'';
$string['deleteinteractionsure'] = '你確定你希望這樣做? 此操作不可回復。';
$string['interactiondeleted'] = '%s 刪除成功';
$string['addnewinteraction'] = '新增 %s';
$string['title'] = '標題';
$string['Role'] = '角色';
$string['changerole'] = '更改角色';
$string['changeroleofuseringroup'] = '更改 %s 於 %s 的角色';
$string['currentrole'] = '現時角色';
$string['changeroleto'] = '更新角色至';
$string['rolechanged'] = '角色已更改';
$string['removefromgroup'] = '從群組移除';
$string['userremoved'] = '用戶已移除';
$string['About'] = '關於';

$string['Joined'] = '已加入';
?>
