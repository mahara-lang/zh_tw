<?php
/**
 * Mahara: Electronic portfolio，weblog，resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation，either version 3 of the License，or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not，see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['administration'] = '管理';

// Installer
$string['installation'] = '安裝';
$string['release'] = '發佈 %s (%s)';
$string['copyright'] = 'Copyright &copy; 2006 onwards，Catalyst IT Ltd';
$string['agreelicense'] = '我同意';
$string['component'] = '組件或插件';
$string['continue'] = '繼續';
$string['coredata'] = '核心數據';
$string['coredatasuccess'] = '成功安裝核心數據';
$string['databasenotutf8'] = '您使用的不是UTF - 8的數據庫。UTF - 8是Mahara建議的字符編碼';
$string['fromversion'] = '從版本';
$string['information'] = '信息';
$string['installsuccess'] = '成功安裝版本';
$string['toversion'] =  '到版本';
$string['notinstalled'] = '未安裝';
$string['nothingtoupgrade'] = '沒有東西需要升級';
$string['performinginstallsandupgrades'] = '安裝和升級進行中...';
$string['runupgrade'] = '進行升級';
$string['successfullyinstalled'] = '成功安裝Mahara!';
$string['thefollowingupgradesareready'] = '以下升級已就緒:';
$string['registerthismaharasite'] = '註冊此Mahara網站';
$string['upgradeloading'] = '載入中...';
$string['upgrades'] = '升級';
$string['upgradesuccess'] = '成功升級';
$string['upgradesuccesstoversion'] = '成功升級至版本 ';
$string['upgradefailure'] = '升級失敗!';
$string['noupgrades'] = '不需要升級!你已擁有最新版本!';
$string['youcanupgrade'] = '您可以從%s (%s) 到%s (%s)升級Mahara!';
$string['Plugin'] = '插件';
$string['jsrequiredforupgrade'] = '您必須啟用JavaScript才能執行安裝或升級';

// Admin navigation menu
$string['adminhome']      = '管理主頁';
$string['configsite']  = '配置網站';
$string['configusers'] = '管理用戶';
$string['configextensions']   = '管理擴展';
$string['manageinstitutions'] = '管理機構';

// Admin homepage strings
$string['siteoptions']    = '網站選項';
$string['siteoptionsdescription'] = '配置基本的網站選項，如姓名，語言和主題';
$string['editsitepages']     = '編輯網頁';
$string['editsitepagesdescription'] = '修改網站內各頁的內容';
$string['linksandresourcesmenu'] = '連結和資源列表';
$string['linksandresourcesmenudescription'] = '管理連結和連結中的文件和資源列表';
$string['sitefiles']          = '網站檔案';
$string['sitefilesdescription'] = '上傳和管理可放在連結，資源菜單和網站選集的檔案';
$string['siteviews']          = '網站選集';
$string['siteviewsdescription'] = '替整個網站創建和管理選集和選集模板';
$string['networking']          = '網絡';
$string['networkingdescription'] = '為Mahara配置網絡';

$string['staffusers'] = '工作人員用戶';
$string['staffusersdescription'] = '分配工作人員用戶的權限';
$string['adminusers'] = '管理員用戶';
$string['adminusersdescription'] = '指定網站管理員訪問權限';
$string['institutions']   = '機構';
$string['institutiondetails']   = '機構詳細信息';
$string['institutionauth']   = '機構權限';
$string['institutionsdescription'] = '安裝和管理已安裝機構';
$string['adminnotifications'] = '管理通知';
$string['adminnotificationsdescription'] = '管理員如何得到系統通知的概況';
$string['uploadcsv'] = '以CSV格式添加用戶';
$string['uploadcsvdescription'] = '上傳包含新用戶的CSV文件';
$string['usersearch'] = '用戶搜索';
$string['usersearchdescription'] = '搜索所有用戶和對他們執行管理';
$string['usersearchinstructions'] = '您可以通過點擊用戶的名字和姓氏的縮寫來搜索用戶，或者輸入一個名稱在搜索框中。如果您想搜索電子郵件地址，您也可以輸入一個電子郵件地址在搜索框中。';
$string['institutionmembersdescription'] = '連接用戶至機構';
$string['institutionstaffdescription'] = '分配工作人員的權限給用戶';
$string['institutionadminsdescription'] = '分配機構管理員訪問權限';
$string['institutionviews']          = '機構選集';
$string['institutionviewsdescription'] = '創建和管理機構選集和選集模板';
$string['institutionfiles']          = '機構檔案';
$string['institutionfilesdescription'] = '在機構選集中上傳和管理檔案';

$string['pluginadmin'] = '插件管理';
$string['pluginadmindescription'] = '安裝和配置插件';

$string['htmlfilters'] = 'HTML過濾器';
$string['htmlfiltersdescription'] = '啟用新的過濾器作為HTML淨化器';
$string['newfiltersdescription'] = '如果您已經下載了一套新的HTML過濾器，您可以安裝他們的解壓文件到%s 文件夾，然後點擊下面的按鈕';
$string['filtersinstalled'] = '過濾器已安裝';
$string['nofiltersinstalled'] = '沒有安裝HTML過濾器';

// Register your Mahara
$string['Field'] = '欄';
$string['Value'] = '值';
$string['datathatwillbesent'] = '將會傳送的資料';
$string['sendweeklyupdates'] = '傳送每週更新?';
$string['sendweeklyupdatesdescription'] = '如你選擇，你的網站會每星期自動傳送更新訊息至 mahara.org，包括你最新的安裝資料。';
$string['Register'] = '註冊';
$string['registrationfailedtrylater'] = '註冊失敗的錯誤代碼 %s。 請稍後再試。';
$string['registrationsuccessfulthanksforregistering'] = '註冊成功 - 多謝註冊!';
$string['registeryourmaharasite'] = '註冊你的 Mahara 網站';
$string['registeryourmaharasitedetail'] = '
<p>你可使用 <a href="http://mahara.org/">mahara.org</a> 註冊你的Mahara網站。 註冊是免費的，以及可幫助我們在全球各地建立Mahara的安裝基礎。</p>
<p>你可觀看將會傳送至mahara.org的資料 - 不包含可分辨用戶的個人資料。</p>
<p>如你選擇 &quot;傳送每週更新&quot;，Mahara 會每星期自動傳送更新訊息至 mahara.org，包括你最新的安裝資料。</p>
<p>註冊將移除此公告。你可在 <a href="%sadmin/site/options.php">網站選項</a> 頁面選擇是否傳送每週更新。</p>';

// Site options
$string['adminsonly'] = '只限管理員';
$string['allowpublicviews'] = '允許公開選集';
$string['allowpublicviewsdescription'] = '如你選擇"是"，用戶可建立公開的作品集選集，而不限於已登入的用戶。';
$string['allowpublicprofiles'] = '允許公開個人檔案';
$string['allowpublicprofilesdescription'] = '如你選擇"是"，用戶可將個人檔案選集設定為可供公眾瀏覽,而不限於已登入用戶。';
$string['captchaonregisterform'] = '註冊時需要Captcha';
$string['captchaonregisterformdescription'] = '當遞交註冊表格時，需要用戶識別captcha圖片並輸入正確的字母';
$string['captchaoncontactform'] = '連絡我們時需要Captcha';
$string['captchaoncontactformdescription'] = '當遞交連絡我們表格時，需要已登出用戶識別captcha圖片並輸入正確的字母';
$string['defaultaccountinactiveexpire'] = '默認帳戶閒置時間';
$string['defaultaccountinactiveexpiredescription'] = '當用戶已有一段時間沒有登入，而帳戶仍然有效的一段時間';
$string['defaultaccountinactivewarn'] = '警訊閒置/過期時間';
$string['defaultaccountinactivewarndescription'] = '在帳戶快將過期或成為閒置之前，系統將會傳送警告訊息給他們。';
$string['defaultaccountlifetime'] = '默認帳戶使用期';
$string['defaultaccountlifetimedescription'] = '如已設定，用戶帳戶會在一段指定的時間過後過期。';
$string['embeddedcontent'] = '嵌入內容';
$string['embeddedcontentdescription'] = '如你希望用戶可在作品集中崁入影片或其他外來內容,你可選擇信任以下哪個網站。';
$string['Everyone'] = '所有人';
$string['institutionautosuspend'] = '自動暫停已到期的機構';
$string['institutionautosuspenddescription'] = '如果核取，已到期的機構將會自動被暫停';
$string['institutionexpirynotification'] = '機構暫停前的警告時間';
$string['institutionexpirynotificationdescription'] = '在機構期滿前，一個通知信息將會寄給網站和機構的管理員。';
$string['language'] = '語言';
$string['pathtoclam'] = 'clam 的路徑';
$string['pathtoclamdescription'] = 'clamscan或clamdscan的檔案系統路徑';
$string['searchplugin'] = '搜尋插件';
$string['searchplugindescription'] = '搜尋插件使用';
$string['sessionlifetime'] = '使用期時間';
$string['sessionlifetimedescription'] = '當用戶閒置後會自動登出的一段以分鐘計的時間。';
$string['setsiteoptionsfailed'] = '設定 %s 選項失敗';
$string['showselfsearchsideblock'] = '啟用作品集搜尋';
$string['showselfsearchsideblockdescription'] = '顯示「搜尋我的作品集」側欄區塊於網站內「我的作品集」的部份';
$string['showtagssideblock'] = '啟用標籤雲';
$string['showtagssideblockdescription'] = '如果啟用，用戶將會看到一個側欄區塊於網站內「我的作品集」的部份 ，它會列出用戶最常用的標籤';
$string['sitedefault'] = '網站默認';
$string['sitelanguagedescription'] = '網站默認語言';
$string['sitename'] = '網站名稱';
$string['sitenamedescription'] = '網站的名稱會顯示在網站的部分的地方及所發出的電郵中。';
$string['siteoptionspagedescription'] = '你可在這裡設定網站總體性的選項，以取代默認設定。';
$string['siteoptionsset'] = '已更新網站選項。';
$string['sitethemedescription'] = '網站默認主題';
$string['tagssideblockmaxtags'] = '顯示在標籤雲的標籤數量';
$string['tagssideblockmaxtagsdescription'] = '顯示在用戶標籤雲的預設標籤數量';
$string['theme'] = '主題';
$string['trustedsites'] = '可信網站';
$string['updatesiteoptions'] = '更新網站選項';
$string['usersallowedmultipleinstitutions'] = '用戶允許多個機構';
$string['usersallowedmultipleinstitutionsdescription'] = '如你選擇，用戶可在同一時間成為多個機構的會員。';
$string['usersseenewthemeonlogin'] = '用戶將會在下次登入時看到新的主題。';
$string['viruschecking'] = '病毒檢查';
$string['viruscheckingdescription'] = '如你選擇，病毒檢查將會啟動，所有已上傳檔案都需經過ClamAV的檢查。';
$string['whocancreategroups'] = '誰能新增群組';
$string['whocancreategroupsdescription'] = '那些用戶可以新增群組';
$string['whocancreatepublicgroups'] = '誰可建立公開群組';
$string['whocancreatepublicgroupsdescription'] = '如你設定為"是"，用戶可建立公眾可瀏覽的群組。';

// Site content
$string['about']               = '關於';
$string['discardpageedits']    = '放棄這個頁面的變更?';
$string['editsitepagespagedescription'] = '你可在這裡編輯網站的某些頁面的內容，例如主頁 (分別是用以用戶登入及登出的)，及在頁尾連結的頁面。';
$string['home']                = '主頁';
$string['loadsitepagefailed']  = '載入網站頁面失敗';
$string['loggedouthome']       = '登出主頁';
$string['pagename']            = '頁面名稱';
$string['pagesaved']           = '頁面已儲存';
$string['pagetext']            = '頁面內容';
$string['privacy']             = '私隱聲明';
$string['savechanges']         = '儲存變更';
$string['savefailed']          = '儲存失敗';
$string['sitepageloaded']      = '已載入網站頁面';
$string['termsandconditions']  = '使用條款';
$string['uploadcopyright']     = '上傳版權聲明';

// Links and resources menu editor
$string['sitefile']            = '網站檔案';
$string['adminpublicdirname']  = '公開';  // Name of the directory in which to store public admin files
$string['adminpublicdirdescription'] = '可供已登出用戶存取的檔案';
$string['badmenuitemtype']     = '不知名頂目類別';
$string['confirmdeletemenuitem'] = '你真的想刪除這個項目?';
$string['deletingmenuitem']    = '刪除項目中...';
$string['deletefailed']        = '刪除項目失敗';
$string['externallink']        = '外部連結';
$string['editmenus']           = '編輯連結及資源';
$string['linkedto']            = '連結至';
$string['linksandresourcesmenupagedescription'] = '連結及資源選單顯示於大部分網頁。 你可加入其他網站及已上傳檔案的連結至 %s管理人員檔案%s 部分.';
$string['loadingmenuitems']    = '載入項目中';
$string['loadmenuitemsfailed'] = '載入項目失敗';
$string['loggedinmenu']        = '已登入連結及資源';
$string['loggedoutmenu']       = '公開連結及資源';
$string['menuitemdeleted']     = '項目已刪除';
$string['menuitemsaved']       = '頂目已儲存';
$string['menuitemsloaded']     = '項目已載入';
$string['name']                = '名稱';
$string['nositefiles']         = '無可用的網站檔案';
$string['public']              = '公開';
$string['savingmenuitem']      = '儲存項目中';
$string['type']                = '類別';

// Admin Files
$string['adminfilespagedescription'] = '你可在這裡上傳檔案，而檔案可包含在 %s連結及資源%s. 在個人目錄中的檔案可加於已登入選單中，而公開目錄中的檔案則可加於公開選單中。';

// Networking options
$string['networkingextensionsmissing'] = '對不起，你不可設置Mahara的網絡，因為你的PHP安裝缺少了一個或多於一個必需的伸展部分。:';
$string['publickey'] = '公開密鑰';
$string['publickeydescription2'] = '這個公開密鑰是自動產生的，及會每 %s 天循環。';
$string['publickeyexpires'] = '公開密鑰期滿';
$string['enablenetworkingdescription'] = '允許你的Mahara伺服器與運作Moodle及其他應用程式的伺服器通訊。';
$string['enablenetworking'] = '啟動網絡';
$string['networkingenabled'] = '網絡已啟動。 ';
$string['networkingdisabled'] = '網絡已停用。 ';
$string['networkingpagedescription'] = 'Mahara的網絡特點讓它跟Mahara及Moodle網站通訊，並在同一部機器上運作。 如網絡已停用，你可設置讓用戶只可在Moodle或Mahara其中一個網站中登入。';
$string['networkingunchanged'] = '網絡設定不變';
$string['promiscuousmode'] = '自動註冊所有主機';
$string['promiscuousmodedisabled'] = '自動註冊已停用. ';
$string['promiscuousmodeenabled'] = '自動註冊已啟動. ';
$string['promiscuousmodedescription'] = '建立機構紀錄以記錄連接的主機，及允許它的用戶登入Mahara';
$string['wwwroot'] = '萬維網根目錄 (WWW Root)';
$string['wwwrootdescription'] = '這是你的用戶存取Mahara安裝程式的URL，及因此而產生的URL及SSL密鑰';
$string['proxysettings'] = '代理設定';
$string['proxyaddress'] = '代理地址';
$string['proxyaddressdescription'] = '如你的網站使用代理伺服器存取網路，在<em>hostname:portnumber</em>指明該代理伺服器作標記';
$string['proxyaddressset'] = '代理伺服器地址組合';
$string['proxyauthmodel'] = '代理伺服器認證樣式';
$string['proxyauthmodeldescription'] = '選擇你的代理伺服器認證樣式，如適用。';
$string['proxyauthmodelset'] = '已設定代理伺服器認證樣式Proxy authentication model has been set';
$string['proxyauthcredentials'] = '代理伺服器憑證';
$string['proxyauthcredentialsdescription'] = '以<em>用戶名:密碼</em>格式輸入所需的憑證，以供代理伺服器通過認證你的網站伺服器。';
$string['proxyauthcredntialsset'] = '代理伺服器認證憑證組合';


// Upload CSV
$string['csvfile'] = 'CSV檔案';
$string['emailusersaboutnewaccount'] = '向用戶發出關於帳戶的電郵?';
$string['emailusersaboutnewaccountdescription'] = '是否傳送電郵至用戶，以通知他們有關新帳戶的資料。';
$string['forceuserstochangepassword'] = '強制更改密碼?';
$string['forceuserstochangepassworddescription'] = '是否強制要求用戶在第一次登入的時候更改他們的密碼。';
$string['uploadcsvinstitution'] = '新用戶的機構及認證方法';
$string['configureauthplugin'] = '你必須在增加新用戶前配置一個認證插件';
$string['csvfiledescription'] = '存有待加用戶的檔案';
$string['uploadcsverrorinvalidfieldname'] = '範疇名稱 "%s" 無效';
$string['uploadcsverrorrequiredfieldnotspecified'] = '所需範疇 "%s" 沒有格式列中指明';
$string['uploadcsverrornorecords'] = '此檔案看來沒有紀錄(雖然頁首是良好的)';
$string['uploadcsverrorunspecifiedproblem'] = '由於某於原因,你的CSV 檔案的紀錄不能插入。如你的檔案格式正確，那是檔案錯誤。你必須  <a href="https://eduforge.org/tracker/?func=add&group_id=176&atid=739">create a bug report</a>，附加CSV檔案 (請記得將密碼刪去) 以及，如有可能，錯誤日誌檔案';
$string['uploadcsverrorinvalidemail'] = '你的檔案中第 %s 列出現錯誤: 此用戶的電郵地址形式不正確';
$string['uploadcsverrorincorrectnumberoffields'] = '你的檔案中第 %s 列出現錯誤: 這列的範疇數目不正確';
$string['uploadcsverrorinvalidpassword'] = '你的檔案中第 %s 列出現錯誤: 此用戶的密碼形式不正確';
$string['uploadcsverrorinvalidusername'] = '你的檔案中第 %s 列出現錯誤: 此用戶的用戶名稱形式不正確';
$string['uploadcsverrormandatoryfieldnotspecified'] = '你的檔案中第 %s 列沒有所需的 "%s" 範疇';
$string['uploadcsverroruseralreadyexists'] = '你的檔案中第 %s 列指明用戶名稱 "%s" 已存在';
$string['uploadcsverroremailaddresstaken'] = '你的檔案中第 %s 列指明電郵地址 "%s" 已被其他用戶使用';
$string['uploadcsvpagedescription2'] = '<p>你可使用此用具，透過<acronym title="Comma Separated Values">CSV</acronym> 檔案上傳新用戶</p>
   
<p>你的CSV檔案中第一列必須指明你的CSV數據的格式。 舉例來說，它必須顯示為:</p>

<pre>用戶名稱,密碼,電郵地址,名字,姓氏,學生帳戶名稱</pre>

<p>此列必須包含 <tt>用戶名稱</tt>，<tt>密碼</tt>，<tt>電郵地址</tt>，<tt>名字</tt> 及 <tt>姓氏</tt> 範疇。 此外亦必須包含你設定為強制用戶輸入的範疇，及任何為你上傳的用戶所屬的機構而封鎖的範疇。 你可<a href="%s">為所有機構配置你的強制範疇</a> ，或 <a href="%s">配置每一個機構的封鎖範疇</a>。</p>

<p>你的CSV檔案可包含其他你所需要的個人檔案範疇。 以下為所有範疇的列表:</p>

%s';
$string['uploadcsvpagedescription2institutionaladmin'] = '<p>你可使用此用具，透過<acronym title="Comma Separated Values">CSV</acronym>檔案上傳新用戶.</p>

<p>你的CSV檔案中第一列必須指明你的CSV數據的格式。 舉例來說，它必須顯示為:</p>

<pre>用戶名稱,密碼,電郵地址,名字,姓氏,學生帳戶名稱</pre>

<p>此列必須包含 <tt>用戶名稱</tt>，<tt>密碼</tt>，<tt>電郵地址</tt>，<tt>名字</tt> 及 <tt>姓氏</tt> 範疇。 此外亦必須包含網站管理員設定為強制用戶輸入的範疇，及任何為你上傳的用戶所屬的機構而封鎖的範疇。 你可<a href="%s">為你所管理的機構配置你的強制範疇</a> ,</p>

<p>你的CSV檔案可包含其他你所需要的個人檔案範疇。 以下為所有範疇的列表:</p>

%s';
$string['uploadcsvsomeuserscouldnotbeemailed'] = '某些用戶不能接收電郵。 他們的電郵地址可能是無效的，或Mahara運作的伺服器並不是配置為可正確地傳送電郵。 伺服器錯誤日誌會有更多資訊。 現在，你或許需要手動聯絡這些人。:';
$string['uploadcsvusersaddedsuccessfully'] = '成功增加檔案中的用戶';
$string['uploadcsvfailedusersexceedmaxallowed'] = '沒有增加任何戶，這是由於檔案中有太多用戶。  機構中的用戶數目會超過允許的最大數目。';

// Admin Users
$string['adminuserspagedescription'] = '<p>在這裡你可選擇哪些用戶為網站管理人員。 右邊列出的是現有的管理人員，左邊列出的則是潛在管理人員。</p><p>系統必須有至少一名管理員。</p>';
$string['institutionadminuserspagedescription'] = '在這裡你可選擇哪些用戶為機構管理人員。 右邊列出的是現有的管理人員，左邊列出的則是潛在管理人員。';
$string['potentialadmins'] = '潛在管理人員';
$string['currentadmins'] = '現有管理人員';
$string['adminusersupdated'] = '已更新管理用戶';

// Staff Users
$string['staffuserspagedescription'] = '在這裡你可選擇哪些用戶為網站工作人員。 右邊列出的是現有的工作人員，左邊列出的則是潛在工作人員。';
$string['institutionstaffuserspagedescription'] = '在這裡你可選擇哪些用戶為機構工作人員。 右邊列出的是現有的工作人員，左邊列出的則是潛在工作人員。';
$string['potentialstaff'] = '潛在工作人員';
$string['currentstaff'] = '現有工作人員';
$string['staffusersupdated'] = '已更新工作人員用戶';

// Admin Notifications

// Suspended Users
$string['deleteusers'] = '刪除用戶';
$string['deleteuser'] = '刪除用戶';
$string['confirmdeleteusers'] = '你確定你希望刪除所選擇的用戶??';
$string['exportingnotsupportedyet'] = '未支援輸出用戶個人檔案';
$string['exportuserprofiles'] = '輸出用戶個人檔案';
$string['nousersselected'] = '沒有選擇任合用戶';
$string['suspenduser'] = '中止用戶';
$string['suspendedusers'] = '被中止用戶';
$string['suspensionreason'] = '中止原因';
$string['errorwhilesuspending'] = '嘗試中止時發生錯誤';
$string['suspendedusersdescription'] = '中止或恢復用戶使用網站';
$string['unsuspendusers'] = '恢復用戶';
$string['usersdeletedsuccessfully'] = '刪除用戶成功';
$string['usersunsuspendedsuccessfully'] = '恢復用戶成功';
$string['suspendingadmin'] = '正在中止管理人員';
$string['usersuspended'] = '用戶已中止';
$string['userunsuspended'] = '用戶已恢復';

// User account settings
$string['accountsettings'] = '帳戶設定';
$string['siteaccountsettings'] = '網站帳戶設定';
$string['resetpassword'] = '重設密碼';
$string['resetpassworddescription'] = '如你在這裡輸入文字，用戶密碼會被取代。';
$string['forcepasswordchange'] = '在下次登入時強制更改密碼';
$string['forcepasswordchangedescription'] = '在下次登入時，用戶會被指示至更改密碼頁面。';
$string['sitestaff'] = '網站工作人員';
$string['siteadmins'] = '網站管理人員';
$string['siteadmin'] = '網站管理員';
$string['accountexpiry'] = '帳戶過期';
$string['accountexpirydescription'] = '用戶被取消登入資格的日期';
$string['suspended'] = '被中止';
$string['suspendedreason'] = '中止原因';
$string['suspendedreasondescription'] = '當用戶下次嘗試登入時系統會顯示的文字。';
$string['unsuspenduser'] = '已恢復的用戶';
$string['thisuserissuspended'] = '此用戶已被中止';
$string['suspendedby'] = '此用戶已被 %s 中止';
$string['deleteuser'] = '刪除用戶';
$string['userdeletedsuccessfully'] = '刪除用戶成功';
$string['confirmdeleteuser'] = '你確定你希望刪除此用戶?';
$string['filequota'] = '檔案限額 (MB)';
$string['filequotadescription'] = '用戶檔案區域的總容量。';
$string['addusertoinstitution'] = '增加用戶至機構';
$string['removeuserfrominstitution'] = '將用戶從機構移除';
$string['confirmremoveuserfrominstitution'] = '你確定你希望將用戶從此機構移除?';
$string['usereditdescription'] = '在這裡你可檢視及設定此用戶的帳戶資料。 以下你並可 <a href="#suspend">中止或刪除此帳戶</a>，或更改此用戶在所屬 <a href="#institutions">機構的設定</a>。';
$string['suspenddeleteuser'] = '中止/刪除用戶';
$string['suspenddeleteuserdescription'] = '在這裡你可中止或徹底刪除用戶的帳戶。 被中止的用戶不能再登入，直至他們的帳戶被恢復。 請注意中止可被回復，但刪除卻 <strong>不可</strong> 被回復。';
$string['deleteusernote'] = '請注意此操作 <strong>不可被回復</strong>。';

// Add User
$string['adduser'] = '增加用戶';
$string['adduserdescription'] = '建立新用戶';
$string['adduserpagedescription'] = '<p>在這裡你可增加新用戶至系統。 當已增加時,他們會收到電郵有關他們的新帳戶的通知，包括他們的用戶名稱及密碼。 他們會在第一次登入時被要求更改密碼。</p>';
$string['createuser'] = '建立用戶';
$string['newuseremailnotsent'] = '傳送歡迎電郵至新用戶失敗。';

// Login as
$string['loginasuser'] = '以 %s 身份登入';
$string['becomeadminagain'] = '再次成為 %s ';
// Login-as exceptions
$string['loginasdenied'] = '在沒有許可的情況下，嘗試登入為另一名用戶';
$string['loginastwice'] = '在已登入為另一名用戶的情況下，嘗試登入為另一名用戶';
$string['loginasrestorenodata'] = '沒有用戶資料需要復原';
$string['loginasoverridepasswordchange'] = '當你冒充為另一名用戶時，你可選擇  %s不管怎樣都登入%s，忽略更改密碼的頁面。';

// Institutions
$string['Add'] = '增加';
$string['admininstitutions'] = '管理機構';
$string['adminauthorities'] = '管理授權者';
$string['addinstitution'] = '增加機構';
$string['authplugin'] = '認證插件';
$string['deleteinstitution'] = '刪除機構';
$string['deleteinstitutionconfirm'] = '你確認你希望刪除此機構?';
$string['institutionaddedsuccessfully2'] = '增加機構成功';
$string['institutiondeletedsuccessfully'] = '刪除機構成功';
$string['noauthpluginforinstitution'] = '你的網站管理員沒有為此機構配置認證插件。';
$string['adminnoauthpluginforinstitution'] = '請為此機構配置認證插件。';
$string['institutionname'] = '機構名稱';
$string['institutionnamealreadytaken'] = '此機構名稱已被使用';
$string['institutiondisplayname'] = '機構顯示名稱';
$string['institutionupdatedsuccessfully'] = '機構更新成功。';
$string['registrationallowed'] = '註冊允許?';
$string['registrationalloweddescription2'] = '用戶是否可以使用註冊表格以註冊此網站及機構';
$string['defaultmembershipperiod'] = '默認會員身份期間';
$string['defaultmembershipperioddescription'] = '新用戶跟機構維持聯繫的時間有多久';
$string['authenticatedby'] = '認證方法';
$string['authenticatedbydescription'] = '此用戶能過Mahara認證的時間有多久';
$string['remoteusername'] = '外部認證的用戶名稱';
$string['remoteusernamedescription'] = '用戶在遠端系統中的用戶名稱';
$string['institutionsettings'] = '機構設定';
$string['institutionsettingsdescription'] = '在這裡你可更改此用戶在有關機構的會員身份設定。';
$string['changeinstitution'] = '更改機構';
$string['institutionstaff'] = '機構工作人員';
$string['institutionadmins'] = '機構管理員';
$string['institutionadmin'] = '機構管理人員';
$string['institutionadministrator'] = '機構管理員';
$string['institutionadmindescription'] = '如已設定，用戶可管理此機構的所有用戶。';
$string['settingsfor'] = '有關設定:';
$string['institutionadministration'] = '機構的管理';
$string['institutionmembers'] = '機構會員';
$string['notadminforinstitution'] = '你不是該機構的管理員';
$string['institutionmemberspagedescription'] = '在這頁面你可看到要求成為會員的用戶，你可增加他們至你的機構。  你並可從機構移除用戶，及邀請用戶加入。';

$string['institutionusersinstructionsrequesters'] = '用戶列表的左邊顯示所有要求加入你的機構的用戶。 你可使用搜尋空格以減少用戶的顯示。 如你希望增加用戶至機構，或拒絕他們的會員要求，首先在左邊選擇一個或更多用戶以將他們移動至右邊及點擊向右的箭頭。 你所選擇的用戶會移動至右邊。 點擊"增加會員" 按鈕會增加所有右邊的用戶至機構。 點擊"拒絕要求" 按鈕會移除所有右邊的用戶的會員身份要求。';
$string['institutionusersinstructionsnonmembers'] = '用戶列表的左邊顯示所有還未成為機構的會員的用戶。 你可使用搜尋空格以減少用戶的顯示。 如你希望邀請用戶加入機構，首先在左邊選擇一個或更多用戶以將他們移動至右邊及點擊向右的箭頭。 你所選擇的用戶會移動至右邊。 點擊"邀請用戶" 按鈕會傳送邀請至所有右邊的用戶。  這些用戶不會跟機構有任何聯繫，直至他們接受邀請為止。';
$string['institutionusersinstructionsmembers'] = '用戶列表的左邊顯示機構的所有會員。  你可使用搜尋空格以減少用戶的顯示。  如你希望從機構中移除會員，首先在左邊選擇一個或更多用戶以將他們移動至右邊及點擊向右的箭頭。 你所選擇的用戶會移動至右邊。 點擊"移除用戶" 按鈕會從機構中移除所有用戶。 左邊的用戶會保留在機構。';

$string['editmembers'] = '編輯會員';
$string['editstaff'] = '編輯工作人員';
$string['editadmins'] = '編輯管理人員';
$string['membershipexpiry'] = '會員身份過期';
$string['membershipexpirydescription'] = '用戶從機構被自動移除的日期';
$string['studentid'] = '身份號碼';
$string['institutionstudentiddescription'] = '機構非必須的識別碼。 用戶不可編輯此範疇。';

$string['userstodisplay'] = '顯示的用戶:';
$string['institutionusersrequesters'] = '要求機構會員身份的人';
$string['institutionusersnonmembers'] = '還未要求會員身份的人';
$string['institutionusersmembers'] = '已是機構會員的人';

$string['addnewmembers'] = '增加新會員';
$string['addnewmembersdescription'] = '';
$string['usersrequested'] = '已要求會員身份的用戶';
$string['userstobeadded'] = '已被加為會員的用戶';
$string['userstoaddorreject'] = '等待被加/拒絕的用戶';
$string['addmembers'] = '增加會員';
$string['inviteuserstojoin'] = '邀請用戶加入機構';
$string['Non-members'] = '非會員';
$string['userstobeinvited'] = '將被邀請的用戶';
$string['inviteusers'] = '邀請用戶';
$string['removeusersfrominstitution'] = '從機構移除用戶';
$string['currentmembers'] = '現有會員';
$string['userstoberemoved'] = '將被移除的用戶';
$string['removeusers'] = '移除用戶';
$string['declinerequests'] = '拒絕要求';
$string['nousersupdated'] = '沒有用戶更新';

$string['institutionusersupdated_addUserAsMember'] = '用戶已增加';
$string['institutionusersupdated_declineRequestFromUser'] = '要求已拒絕';
$string['institutionusersupdated_removeMembers'] = '用戶已移除';
$string['institutionusersupdated_inviteUser'] = '邀請已傳送';

$string['maxuseraccounts'] = '最大用戶帳戶數目允許';
$string['maxuseraccountsdescription'] = '與機構有聯繫的用戶帳戶的最大數目。  如沒限制，此範疇會留空。';
$string['institutionuserserrortoomanyusers'] = '用戶沒有被加。  會員數目不可超過此機構的最大限額。  你可增加較少會員，從機構中移除某些會員，或要求網站管理員增加會員數目的最大限額。';
$string['institutionuserserrortoomanyinvites'] = '你的邀請尚未送出。  現存會員數目加上未完成的邀請數目不可超過此機構的最大限額。  你可增加較少會員，從機構中移除某些會員，或要求網站管理員增加會員數目的最大限額。';

$string['Members'] = '會員';
$string['Maximum'] = '最大值';
$string['Staff'] = '工作人員';
$string['Admins'] = '管理人員';

$string['noinstitutions'] = '無機構';
$string['noinstitutionsdescription'] = '如你希望將用戶聯繫至機構，你必須先建立機構。';

$string['Lockedfields'] = '已封鎖範疇';

// Suspend Institutions
$string['errorwhileunsuspending'] = '當嘗試暫信時發生了一個錯誤';
$string['institutionsuspended'] = '暫停機構';
$string['institutionunsuspended'] = '重啟機構';
$string['suspendedinstitution'] = '已暫停';
$string['suspendinstitution'] = '暫停機構';
$string['suspendinstitutiondescription'] = '在這裡你可以暫停一個機構，被暫停機構內的用戶將會不能夠登入，直至機構被重啟。';
$string['suspendedinstitutionmessage'] = '這個機構已被暫停';
$string['unsuspendinstitution'] = '重啟機構';
$string['unsuspendinstitutiondescription'] = '在這裡你可以重啟一個機構。被暫停機構內的用戶將會不能夠登入，直至機構被重啟。<br /><strong>注意:</strong> 在沒有重設或關閉期滿日之下，重啟一個機構可能會令機構在下一日再被暫停。';
$string['unsuspendinstitutiondescription_top'] = '<em>注意:</em> 在沒有重設或關閉期滿日之下，重啟一個機構可能會令機構在下一日再被暫停。';

// Admin User Search
$string['Query'] = '詢問';
$string['Institution'] = '機構';
$string['confirm'] = '確認';
$string['invitedby'] = '被...邀請';
$string['requestto'] = '要求至';
$string['useradded'] = '已加入用戶';
$string['invitationsent'] = '已傳送邀請';

// general stuff
$string['notificationssaved'] = '通知設定已儲存';
$string['onlyshowingfirst'] = '只是先顯示';
$string['resultsof'] = '...的結果 ';

$string['installed'] = '已安裝';
$string['errors'] = '錯誤';
$string['install'] = '安裝';
$string['reinstall'] = '重新安裝';
?>