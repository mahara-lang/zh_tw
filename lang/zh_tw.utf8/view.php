<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['createview']             = '建立選集';
$string['createviewstepone']      = '建立選集第一步: 選集設計';
$string['createviewsteptwo']      = '建立選集第二步: 資料';
$string['createviewstepthree']    = '建立選集第三步: 存取';
$string['createtemplate']         = '建立範本';
$string['editviewdetails']        = '編輯選集 "%s" 的資料';
$string['editblocksforview']      = '編輯選集 "%s"';
$string['editaccessforview']      = '編輯選集 "%s" 的存取';
$string['next']                   = '下一步';
$string['back']                   = '上一步';
$string['title']                  = '選集標題';
$string['description']            = '選集敘述';
$string['startdate']              = '存取開始日期/時間';
$string['stopdate']               = '存取結束日期/時間';
$string['startdatemustbebeforestopdate'] = '開始的日期必須在結束日期之前';
$string['ownerformat']            = '名稱顯示格式';
$string['ownerformatdescription'] = '你想觀看選集的人看到的名稱是怎樣的?';
$string['profileviewtitle']       = '個人檔案選集';
$string['editprofileview']        = '編輯個人檔案選集';

// my views
$string['artefacts'] = '作品';
$string['myviews'] = '我的選集';
$string['groupviews'] = '群組選集';
$string['institutionviews'] = '機構選集';
$string['reallyaddaccesstoemptyview'] = '你的選集沒有作品。 你確認你希望讓這些用戶存取你的選集?';
$string['viewdeleted'] = '選集已刪除';
$string['viewsubmitted'] = '選集已提交';
$string['editviewnameanddescription'] = '編輯選集資料';
$string['editviewaccess'] = '編輯選集存取';
$string['deletethisview'] = '刪除此選集';
$string['submitthisviewto'] = '提交此選集至';
$string['forassessment'] = '作評估';
$string['accessfromdate2'] = '沒有人可在 %s 之前看到此選集';
$string['accessuntildate2'] = '沒有人可在 %s 之後看到此選集';
$string['accessbetweendates2'] = '沒有人可在 %s 之前或 %s 之後看到此選集';
$string['artefactsinthisview'] = '此選集的作品';
$string['whocanseethisview'] = '誰可看到此選集';
$string['view'] = '選集';
$string['views'] = '選集';
$string['View'] = '選集';
$string['Views'] = '選集';
$string['viewsubmittedtogroup'] = '此選集已提交至 <a href="%sgroup/view.php?id=%s">%s</a>';
$string['nobodycanseethisview2'] = '只有你可看到此選集';
$string['noviews'] = '無選集。';
$string['youhavenoviews'] = '你沒有選集。';
$string['viewsownedbygroup'] = '此群組所擁有的選集';
$string['viewssharedtogroup'] = '在此群組分享的選集';
$string['viewssharedtogroupbyothers'] = '其他人在此群組分享的選集';
$string['viewssubmittedtogroup'] = '提交至此群組的選集';

// access levels
$string['public'] = '公開';
$string['loggedin'] = '已登入用戶';
$string['friends'] = '朋友';
$string['groups'] = '群組';
$string['users'] = '用戶';
$string['friendslower'] = '朋友';
$string['grouplower'] = '群組';
$string['tutors'] = '導師';
$string['loggedinlower'] = '已登入用戶';
$string['publiclower'] = '公開';
$string['everyoneingroup'] = '群組內所有人';
$string['token'] = '隱藏URL';

// view user
$string['inviteusertojoingroup'] = '邀請此用戶加入某群組';
$string['addusertogroup'] = '增加此用戶至某群組';

// view view
$string['addedtowatchlist'] = '此選集已增加至你的觀看名單';
$string['attachment'] = '附件';
$string['removedfromwatchlist'] = '此選集已從你的觀看名單中移除';
$string['addfeedbackfailed'] = '增加回應失敗';
$string['addtowatchlist'] = '增加選集至觀看名單';
$string['removefromwatchlist'] = '從觀看名單中移除選集';
$string['alreadyinwatchlist'] = '此選集已在你的觀看名單中';
$string['attachedfileaddedtofolder'] = "附加的檔案 %s 已加至你的 '%s' 文件夾。";
$string['attachfile'] = "附加檔案";
$string['complaint'] = '投訴';
$string['date'] = '日期';
$string['feedback'] = '回應';
$string['feedbackattachdirname'] = '評估檔案';
$string['feedbackattachdirdesc'] = '檢視評估的附加檔案';
$string['feedbackattachmessage'] = '附加檔案已加至你的 %s 文件夾';
$string['feedbackonthisartefactwillbeprivate'] = '此作品的回應只會顯示予擁有人。';
$string['feedbackonviewbytutorofgroup'] = '在%s中%s的%s的回應';
$string['feedbacksubmitted'] = '已提交的回應';
$string['makepublic'] = '設為公開';
$string['nopublicfeedback'] = '無公開回應';
$string['notifysiteadministrator'] = '通知網站管理員';
$string['placefeedback'] = '發放回應';
$string['placefeedbacknotallowed'] = '你不可在此選集發放回應';
$string['print'] = '列印';
$string['thisfeedbackispublic'] = '這是公開的回應';
$string['thisfeedbackisprivate'] = '這是私人的回應';
$string['makeprivate'] = '更改為私人的';
$string['reportobjectionablematerial'] = '報告不良資料';
$string['reportsent'] = '你的報告已傳送';
$string['updatewatchlistfailed'] = '觀看名單更新失敗';
$string['watchlistupdated'] = '你的觀看名單已更新';
$string['editmyview'] = '編輯我的選集';
$string['backtocreatemyview'] = '回到建立我的選集';

$string['friend'] = '朋友';
$string['profileicon'] = '個人檔案圖像';

// general views stuff
$string['Added'] = '已增加';
$string['allviews'] = '所有選集';

$string['submitviewconfirm'] = '如你提交 \'%s\' 至 \'%s\' 作評估, 你將不能再編輯你的選集, 直至你的導師完成評估。你確定你希望現在提交此選集?  ';
$string['viewsubmitted'] = '選集已提交';
$string['submitviewtogroup'] = '提交 \'%s\' 至 \'%s\' 作評估';
$string['cantsubmitviewtogroup'] = '你不可提交此選集至此群組作評估';

$string['cantdeleteview'] = '你不可刪除此選集';
$string['deletespecifiedview'] = '刪除選集 "%s"';
$string['deleteviewconfirm'] = '你確定你希望刪除此選集? 此操作不可回復.';

$string['editaccesspagedescription2'] = '<p>根據默認設定, 只有你可看到此選集。 在這裡你可選擇你希望還有誰可看到此選集內的資料。 點擊增加以設定授權存取予公眾、已登入用戶或朋友。 使用搜尋空格以增加個別用戶或群組。 已增加的用戶會顯示於右邊方格內, 在"已增加"之下。</p>
<p>你可允許其他用戶複製你的選集至他們的個人作品集。當用戶複製某個選集, 他們會自動得到當中的檔案及文件夾的複製檔案。</p>
<p>當你完成操作, 拉下卷軸及點擊儲存以繼續。</p>';

$string['overridingstartstopdate'] = '優先開始/結束日期';
$string['overridingstartstopdatesdescription'] = '如有需要, 你可設定優先開始及/或結束日期。 其他人在開始日期前及結束日期後將不能觀看你的選集, 即使你已授權存取。';

$string['emptylabel'] = '點擊這裡以輸入文字作標記';
$string['empty_block'] = '從左邊的樹狀圖形中選取作品, 然後放在這裡';

$string['viewinformationsaved'] = '選集資料儲存成功';

$string['canteditdontown'] = '你不可編輯此選集因為你不是擁有者';
$string['canteditdontownfeedback'] = '你不可編輯此回應因為你不是擁有者';
$string['canteditsubmitted'] = '你不可編輯此選集因為此選集已提交至 "%s" 作評估。 你將要等候直至導師發回你的選集。';
$string['feedbackchangedtoprivate'] = '回應更改為私人的';

$string['addtutors'] = '增加導師';
$string['viewcreatedsuccessfully'] = '建立選集成功';
$string['viewaccesseditedsuccessfully'] = '選集存取儲存成功';
$string['viewsavedsuccessfully'] = '選集儲存成功';

$string['invalidcolumn'] = '欄 %s 的數目超過限額';

$string['confirmcancelcreatingview'] = '此選集還未完成。 你確定要取消?';

// view control stuff

$string['editblockspagedescription'] = '<p>在以下的活頁中選擇你希望顯示於選集的作品。你可將作品拉下至選集的版面。選擇 ? 圖像以了解更多資訊。</p>';
$string['displaymyview'] = '顯示我的選集';
$string['editthisview'] = '編輯此選集';

$string['success.addblocktype'] = '加入作品成功';
$string['err.addblocktype'] = '不可加入作品至你的選集';
$string['success.moveblockinstance'] = '移動作品成功';
$string['err.moveblockinstance'] = '不可將作品移動至指定位置';
$string['success.removeblockinstance'] = '刪除作品成功';
$string['err.removeblockinstance'] = '不可刪除作品';
$string['success.addcolumn'] = '加入欄位成功';
$string['err.addcolumn'] = '增加欄位失敗';
$string['success.removecolumn'] = '欄位刪除成功';
$string['err.removecolumn'] = '刪除欄位失敗';

$string['confirmdeleteblockinstance'] = '你確定你希望刪除此作品?';
$string['blockinstanceconfiguredsuccessfully'] = '配置作品成功';

$string['blocksintructionnoajax'] = '選擇作品及在選集中加入的位置。 你可使用標題列中的箭頭按鈕以放置作品。';
$string['blocksinstructionajax'] = '將作品拉在此列底下以加入選集版面中。 你可在選集版面中拉動作品以放置在適合的位置。';

$string['addnewblockhere'] = '在這裡加入作品';
$string['add'] = '增加';
$string['addcolumn'] = '增加欄位';
$string['removecolumn'] = '移除此欄';
$string['moveblockleft'] = '向左移動此作品';
$string['moveblockdown'] = '向下移動此作品';
$string['moveblockup'] = '向上移動此作品';
$string['moveblockright'] = '向右移動此作品';
$string['Configure'] = '配置';
$string['configureblock'] = '配置此作品';
$string['removeblock'] = '移除此作品';
$string['blocktitle'] = '作品標題';

$string['changemyviewlayout'] = '更改我的選集版面';
$string['viewcolumnspagedescription'] = '首先, 選擇你的選集中欄位的數目。接著, 你可更改欄寬。';
$string['viewlayoutpagedescription'] = '選擇你希望欄位在你的選集中版面中的設計。';
$string['changeviewlayout'] = '更改我的選集版面';
$string['backtoyourview'] = '回到我的選集';
$string['viewlayoutchanged'] = '選集版面已更改';
$string['numberofcolumns'] = '欄位數目';


$string['by'] = '由';
$string['in'] = '在';
$string['noblocks'] = '對不起, 這個類別沒有作品 :(';
$string['Preview'] = '預覽';

$string['50,50'] = $string['33,33,33'] = $string['25,25,25,25'] = '相等寬度';
$string['67,33'] = '左欄較大';
$string['33,67'] = '右欄較大';
$string['25,50,25'] = '中間欄位較大';
$string['15,70,15'] = '中間欄位偏大';
$string['20,30,30,20'] = '中間欄位較大';
$string['noviewlayouts'] = '沒有 %s 欄位選集的選集版面';

$string['blocktypecategory.feeds'] = '外部來源';
$string['blocktypecategory.fileimagevideo'] = '檔案, 圖片及影片';
$string['blocktypecategory.general'] = '一般資料';

$string['notitle'] = '無標題';
$string['clickformoreinformation'] = '點擊以了解更多資訊及發放回應';

$string['Browse'] = '瀏覽';
$string['Search'] = '搜尋';
$string['noartefactstochoosefrom'] = '對不起, 沒有可選擇的作品';

$string['access'] = '存取';
$string['noaccesstoview'] = '你沒有得到授權存取此選集';

// Templates
$string['Template'] = '範本';
$string['allowcopying'] = '允許複製';
$string['templatedescription'] = '如希望能觀看你的選集的人可複製你的選集, 在這個空格打鉤。';
$string['choosetemplatepagedescription'] = '<p>在這裡你可搜尋允許你複製的選集, 作為建立新選集的起點。 你可點擊選集的名稱, 以觀看每個選集的預覽。 當你找到希望複製的選集, 點擊相關的"複製選集"按照以複製及開始個人化。</p>';
$string['choosetemplategrouppagedescription'] = '<p>在這裡你可搜尋允許此群組複製的選集, 以作為建立新選集的起點。你可點擊選集的名稱, 以觀看每個選集的預覽。 當你找到希望複製的選集, 點擊相關的"複製選集"按照以複製及開始個人化。</p><p><strong>注意:</strong> 群組一般不可複製博客或博客文章。</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>>在這裡你可搜尋允許此機構複製的選集, 作為建立新選集的起點。 你可點擊選集的名稱, 以觀看每個選集的預覽。 當你找到希望複製的選集, 點擊相關的"複製選集"按照以複製及開始個人化。</p><p><strong>注意:</strong> 機構一般不可複製博客或博客文章。</p>';
$string['copiedblocksandartefactsfromtemplate'] = '已從 %s 複製 %d 作品及 %d 作品';
$string['filescopiedfromviewtemplate'] = '已從 %s 複製檔案';
$string['viewfilesdirname'] = '選集檔案';
$string['viewfilesdirdesc'] = '複製而來的選集的檔案';
$string['thisviewmaybecopied'] = '允許複製';
$string['copythisview'] = '複製此選集';
$string['copyview'] = '複製選集';
$string['createemptyview'] = '建立空白選集';
$string['copyaview'] = '複製選集';
$string['Untitled'] = '無標題';
$string['copyfornewusers'] = '複製予新用戶';
$string['copyfornewusersdescription'] = '當新用戶建立, 自動製造此選集的複製品至用戶的作品集。';
$string['copyfornewmembers'] = '複製予機構新會員';
$string['copyfornewmembersdescription'] = '自動製造此選集的複製品予 %s 的所有新會員。';
$string['copyfornewgroups'] = '複製予新群組';
$string['copyfornewgroupsdescription'] = '自動在這些群組類別中的所有群組製造此選集的複製品:';
$string['searchviews'] = '搜尋選集';
$string['searchowners'] = '搜尋擁有人';
$string['owner'] = '擁有人';
$string['Owner'] = '擁有人';
$string['owners'] = '擁有人';
$string['show'] = '顯示';
$string['searchviewsbyowner'] = '從擁有人搜尋選集:';
$string['selectaviewtocopy'] = '選擇你希望複製的選集:';
$string['listviews'] = '列出選集';
$string['nocopyableviewsfound'] = '沒有可複製的選集';
$string['noownersfound'] = '找不到擁有人';
$string['viewsby'] = ' %s 的選集';
$string['Preview'] = '預覽';
$string['close'] = '關閉';
$string['viewscopiedfornewusersmustbecopyable'] = '在設定供新用戶複製的選集前, 你必須允許複製。';
$string['viewscopiedfornewgroupsmustbecopyable'] = '在設定供新群組複製的選集前, 你必須允許複製。';
$string['copynewusergroupneedsloggedinaccess'] = '複製予新用戶或群組的選集必須授權存取予已登入用戶。';

$string['blockcopypermission'] = '允許作品複製';
$string['blockcopypermissiondesc'] = '如你允許其他用戶複製此選集, 你可選擇複製作品的形式。';

?>